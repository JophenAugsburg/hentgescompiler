// FunctionDefinitionNode.java
// Joseph Hentges

package syntaxTree;

import java.util.ArrayList;
import syntaxTree.ExpressionNodes.VariableNode;
import util.DataType;

/**
 * This represents a single function definition node.
 * 
 * @author Joseph Hentges
 * April 2, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class FunctionDefinitionNode extends SyntaxTreeNode {
    
    private DataType type;
    private String name;
    private ArrayList<VariableNode> parameters = new ArrayList<>();
    
    public void setType(DataType type) {
        this.type = type;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setParameters(ArrayList<VariableNode> parameters) {
        this.parameters = parameters;
    }
    
    public DataType getType() {
        return type;
    }
    
    public String getName() {
        return name;
    }
    
    public ArrayList<VariableNode> getParameters() {
        return parameters;
    }
    
    /**
     * Creates a String representation of this declarations node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Function: " + this.name + "\n";
        for( VariableNode parameter : parameters) {
            answer += parameter.indentedToString( level + 1);
        }
        return answer;
    }
}
