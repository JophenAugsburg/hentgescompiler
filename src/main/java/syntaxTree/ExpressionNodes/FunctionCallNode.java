// SyntaxTree.ExpressionNodes.FunctionCallNode.java
// Joseph Hentges

package syntaxTree.ExpressionNodes;

import java.util.ArrayList;
import syntaxTree.ExpressionNode;

/**
 * This represents a single function call node.
 * 
 * @author Joseph Hentges
 * April 2, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class FunctionCallNode extends ExpressionNode {
    
    private String name;
    private ArrayList<ExpressionNode> arguments = new ArrayList();
    
    public FunctionCallNode(String name) {
        this.name = name;
    }
    
    public void setArguments(ArrayList<ExpressionNode> arguments) {
        this.arguments = arguments;
    }
    
    public ArrayList<ExpressionNode> getArguments() {
        return arguments;
    }
    
    public String getName() {
        return name;
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Function: " + this.name + " | Returns: " + this.getType() + "\n";
        for (int i = 0; i < arguments.size(); i += 1) {
            answer += arguments.get(i).indentedToString(level + 1);
        }
        return( answer);
    }
    
}
