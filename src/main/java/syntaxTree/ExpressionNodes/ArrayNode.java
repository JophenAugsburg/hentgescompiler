/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxTree.ExpressionNodes;

import syntaxTree.ExpressionNode;

/**
 * This represents a single array value getter node.
 * 
 * @author Joseph Hentges
 * April 29, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class ArrayNode extends ExpressionNode {
    
    private String name;
    private ExpressionNode index;
    
    public ArrayNode(String name) {
        this.name = name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setIndex(ExpressionNode index) {
        this.index = index;
    }
    
    public ExpressionNode getIndex() {
        return index;
    }

    @Override
    public String indentedToString(int level) {
        String answer = this.indentation(level);
        answer += "Array: " + this.name + " | Returns: " + this.getType() + "\n";
        answer += index.indentedToString(level + 1);
        return answer;
    }
    
}
