// SyntaxTree.StatementNodes.IfStatementNode.java
// Joseph Hentges

package syntaxTree.StatementNodes;

import syntaxTree.ExpressionNode;
import syntaxTree.StatementNode;

/**
 * Represents an if statement in Almost C.
 * An if statement includes a boolean expression, and two statements.
 * @author Erik Steinmetz
 */
public class IfStatementNode extends StatementNode {
    private ExpressionNode test;
    private CompoundStatementNode thenStatement;
    private CompoundStatementNode elseStatement;

    public ExpressionNode getTest() {
        return test;
    }

    public void setTest(ExpressionNode test) {
        this.test = test;
    }

    public CompoundStatementNode getThenStatement() {
        return thenStatement;
    }

    public void setThenStatement(CompoundStatementNode thenStatement) {
        this.thenStatement = thenStatement;
    }

    public CompoundStatementNode getElseStatement() {
        return elseStatement;
    }

    public void setElseStatement(CompoundStatementNode elseStatement) {
        this.elseStatement = elseStatement;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "If\n";
        answer += this.test.indentedToString( level + 1);
        answer += this.indentation(level + 1) + "Then\n";
        answer += this.thenStatement.indentedToString( level + 2);
        answer += this.indentation(level + 1) + "Else\n";
        answer += this.elseStatement.indentedToString( level + 2);
        return answer;
    }

}
