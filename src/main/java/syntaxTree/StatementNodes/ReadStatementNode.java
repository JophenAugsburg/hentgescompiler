// SyntaxTree.StatementNodes.ReadStatementNode.java
// Joseph Hentges

package syntaxTree.StatementNodes;

import syntaxTree.StatementNode;

/**
 * This represents a single Read statement node.
 * 
 * @author Joseph Hentges
 * April 2, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class ReadStatementNode extends StatementNode {
    
    private String variable;
    
    public void setVariable(String variable) {
        this.variable = variable;
    }
    
    public String getVariable() {
        return variable;
    }
    
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Read: " + this.variable + "\n";
        return answer;
    }
    
}
