// SyntaxTree.StatementNodes.WhileStatementNode.java
// Joseph Hentges

package syntaxTree.StatementNodes;

import syntaxTree.ExpressionNode;
import syntaxTree.StatementNode;

/**
 * This represents a single While statement node.
 * 
 * @author Joseph Hentges
 * April 2, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class WhileStatementNode extends StatementNode {
    
    private ExpressionNode test;
    private CompoundStatementNode body;
    
    public void setTest(ExpressionNode test) {
        this.test = test;
    }
    
    public ExpressionNode getTest() {
        return test;
    }
    
    public void setBody(CompoundStatementNode body) {
        this.body = body;
    }
    
    public CompoundStatementNode getBody() {
        return body;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "While\n";
        answer += this.test.indentedToString( level + 1);
        answer += this.body.indentedToString( level + 1);
        return answer;
    }
    
}
