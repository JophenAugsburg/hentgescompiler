// SyntaxTree.StatementNodes.AssignmentStatementNode.java
// Joseph Hentges

package syntaxTree.StatementNodes;

import syntaxTree.ExpressionNode;
import syntaxTree.ExpressionNodes.VariableNode;
import syntaxTree.StatementNode;

/**
 * Represents a single assignment statement.
 * @author Erik Steinmetz
 */
public class AssignmentStatementNode extends StatementNode {
    
    private ExpressionNode lvalue;
    private ExpressionNode expression;

    public ExpressionNode getLvalue() {
        return lvalue;
    }

    public void setLvalue(ExpressionNode lvalue) {
        this.lvalue = lvalue;
    }

    public ExpressionNode getExpression() {
        return expression;
    }

    public void setExpression(ExpressionNode expression) {
        this.expression = expression;
    }
    

    
    /**
     * Creates a String representation of this assignment statement node 
     * and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Assignment\n";
        answer += this.lvalue.indentedToString( level + 1);
        answer += this.expression.indentedToString( level + 1);
        return answer;
    }
}
