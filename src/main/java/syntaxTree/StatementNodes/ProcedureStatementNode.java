// SyntaxTree.StatementNodes.AssignmentStatementNode.java
// Joseph Hentges

package syntaxTree.StatementNodes;

import java.util.ArrayList;
import syntaxTree.ExpressionNode;
import syntaxTree.StatementNode;

/**
 * This represents a single procedure statement node.
 * 
 * @author Joseph Hentges
 * April 2, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class ProcedureStatementNode extends StatementNode {
    
    private String name;
    private ArrayList<ExpressionNode> parameters;
    
    public ProcedureStatementNode(String name) {
        this.name = name;
    }
    
    public void setParameters(ArrayList<ExpressionNode> parameters) {
        this.parameters = parameters;
    }
    
    public ArrayList<ExpressionNode> getParameters() {
        return parameters;
    }
    
    public String getName() {
        return name;
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Function: " + this.name + "\n";
        for (int i = 0; i < parameters.size(); i += 1) {
            answer += parameters.get(i).indentedToString(level + 1);
        }
        return( answer);
    }
    
    
}
