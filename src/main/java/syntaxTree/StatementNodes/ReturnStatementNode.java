// SyntaxTree.StatementNodes.ReturnStatementNode.java
// Joseph Hentges

package syntaxTree.StatementNodes;

import syntaxTree.ExpressionNode;
import syntaxTree.StatementNode;

/**
 * This represents a single Return statement node.
 * 
 * @author Joseph Hentges
 * April 2, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class ReturnStatementNode extends StatementNode {
    
    ExpressionNode returnBody;
    
    public void setReturnBody(ExpressionNode returnBody) {
        this.returnBody = returnBody;
    }
    
    public ExpressionNode getReturnBody() {
        return returnBody;
    }
     
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Return:\n";
        answer += this.returnBody.indentedToString( level + 1);
        return answer;
    }
    
}
