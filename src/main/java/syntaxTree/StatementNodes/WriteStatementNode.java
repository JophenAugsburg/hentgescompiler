// SyntaxTree.StatementNodes.WriteStatementNode.java
// Joseph Hentges

package syntaxTree.StatementNodes;

import syntaxTree.ExpressionNode;
import syntaxTree.StatementNode;

/**
 * This represents a single Write statement node.
 * 
 * @author Joseph Hentges
 * April 2, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class WriteStatementNode extends StatementNode {
    
    ExpressionNode body;
    
    public void setBody(ExpressionNode body) {
        this.body = body;
    }
    
    public ExpressionNode getBody() {
        return body;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Write:\n";
        answer += this.body.indentedToString( level + 1);
        return answer;
    }
    
}
