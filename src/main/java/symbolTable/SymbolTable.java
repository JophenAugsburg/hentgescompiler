// SymbolTable.java
// Joseph Hentges

package symbolTable;

import util.DataType;
import util.SymbolKind;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This is the Symbol Table class.
 * 
 * @author Joseph Hentges
 * February 27, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

// whether its a function name or variable name

public class SymbolTable {
    
    private HashMap table; 
    private String fileName;
    
    /**
     * This is the constructor.
     * @param fileName : the name of the file being compiled.
     */
    public SymbolTable(String fileName) {
        table = new HashMap<String, TableEntry>();
        this.fileName = fileName;
    }
    
    /**
     * Format and return the name of the file being created.
     * @return : the file name
     */
    public String getSymbolFileName() {
        return fileName.replaceFirst("[.][^.]+$", "") + ".symbols";
    }
    
    /**
     * Override the toString method for this class.
     * Format and beautify the HashMap contained in this class.
     * @return a formatted string of the HashMap
     */
    @Override
    public String toString() {
        String string = "" + table.size() + " Symbols Found for: " + fileName + "\n";
        string += "Symbols exported to: " + fileName + ".symbols\n\n";
        
        ArrayList<String> keyList = new ArrayList<>(table.keySet());
        ArrayList<TableEntry> valueList = new ArrayList<>(table.values());
        String tableSpace = getMaxSpace(keyList);
        
        for  (int i = 0; i < keyList.size(); i += 1) {
            String spaces = formatSpace(tableSpace, keyList.get(i).length());
            string += keyList.get(i) + spaces + ":  " + valueList.get(i).toString() + "\n";
        }
        return string;
    }
    
    /**
     * This finds the longest key in the hashMap, then returns a string of spaces of that length + 2.
     * @param keyList : the list of hashMap keys (strings)
     * @return spaces : a string of spaces "    "
     */
    private String getMaxSpace(ArrayList<String> keyList) {
        int longest = 0;
        for (int i = 0; i < keyList.size(); i += 1) {
            if (keyList.get(i).length() > longest) {
                longest = keyList.get(i).length();
            }
        }
        String spaces = "";
        for (int i = 0; i < longest + 2; i += 1) {
            spaces += " ";
        }
        return spaces;
    }
    
    /**
     * Format the spaces, to remove unneeded spaces.
     * Keep all spaces to the same length.
     * @param spaces : The string of spaces.
     * @param stringLength : the length of the key (string)
     * @return the String of formatted spaces.
     */
    private String formatSpace(String spaces,int stringLength) {
        return spaces.substring(0, spaces.length()-stringLength);
    }
    
    /**
     * Print an error out.
     * @param errorString : String - the error to be sent out.
     */
    private void error(String errorString) {
        throw new RuntimeException("Error: " + errorString);
    }
    
    public void add(String id, SymbolKind kind) {
        if (exists(id)) {
            error("symbol " + id + " already exists in the table.");
        } else {
            table.put(id, new TableEntry(id, kind));
        }
    }
    
    public SymbolKind getKind(String id) {
        TableEntry entry = (TableEntry) table.get(id);
        if (entry == null) {
            return null;
        }
        return entry.kind;
    }
    
    public boolean exists(String id) {
        if (table.get(id) != null)  {
            return true;
        }
        return false;
    }
    
    /**
     * Internal TableEntry Class
     * Acts as the entry to the HashMap.
     */
    private class TableEntry {
        String id;
        SymbolKind kind;
        DataType dataType;
        
        public TableEntry(String id, SymbolKind kind) {
            this.id = id;
            this.kind = kind;
        }
        
        @Override
        public String toString() {
            return kind.toString();
        }
    }
}
