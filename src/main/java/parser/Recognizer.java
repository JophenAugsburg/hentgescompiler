// Recognizer.java
// Joseph Hentges

package parser;

import util.TokenType;
import java.io.FileReader;
import java.io.StringReader;
import scanner.*;
import util.SymbolKind;
import symbolTable.SymbolTable;

/**
 * This is the Recognizer class.
 * The start of the Recursive Descent Parser.
 * 
 * @author Joseph Hentges
 * January 21, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public class Recognizer {
    
    Token lookahead;
    Scanner scanner;
    SymbolTable symbolTable;
    
    // helper global declaration of  the token lexeme
    String tokenLexeme;
    
    /**
     * This is the constructor.
     * 
     * @param stringFile : String - the String to be read, or a file path
     * @param isFile : boolean - whether the string being passed in is a file path or not
     */
    public Recognizer(String stringFile, boolean isFile) {
        if (isFile) {
            try {
                scanner = new Scanner(new FileReader(stringFile));
                symbolTable = new SymbolTable(stringFile);
            } catch(Exception error) {
                error.printStackTrace();
            }
        } else {
            scanner = new Scanner(new StringReader(stringFile));
            symbolTable = new SymbolTable("NOT_FILE");
        }
        
        try {
            lookahead = scanner.nextToken();
        } catch(Exception error) {
            error.printStackTrace();
        }
    }
    
    public SymbolTable getSymbolTable() {
        return symbolTable;
    }
    
    /**
     * Check if the current token has the same type as the expected type.
     * @param expected  : TokenType - the expected token type.
     */
    private void match(TokenType expected) {
        if (lookahead.getType() == expected) {
            try {
                lookahead = scanner.nextToken();
                if (lookahead == null) {
                    lookahead = new Token(TokenType.ENDOFFILE, "End of File");
                }
            } catch(Exception error) {
                error("Scanner exception");
            }
        } else {
            error("Match of " + expected + " found " + lookahead.getType() + " instead.");
        }
    }
    
    /**
     * Print an error out.
     * @param errorString : String - the error to be sent out.
     */
    private void error(String errorString) {
        throw new Error("Error: " + errorString);
    }
    
    /**
     * Determine if the lookahead type  is one of the following:
     * ==, !=, <, >, <=, >=
     * @return the type of the lookahead tokena, null if none.
     */
    private boolean isRelop() {
        switch(lookahead.getType()){
            case EQUALS:
                return true;
            case NOTEQUAL:
                return true;
            case LESSTHAN:
                return true;
            case GREATERTHAN:
                return true;
            case LESSTHANOREQUAL:
                return true;
            case GREATERTHANOREQUAL:
                return true;
            default:
                return false;
        }
    }
    
    /**
     * Determine if the lookahead type  is one of the following:
     * +, -, ||
     * @return whether the lookahead token type is one of those.
     */
    private boolean isAddop() {
        switch(lookahead.getType()){
            case PLUS:
                return true;
            case MINUS:
                return false;
            case OR:
                return true;
            default:
                return false;
        }
    }
    
    /**
     * Determine if the lookahead type  is one of the following:
     * *, /, %, &&
     * @return whether the lookahead token type is one of those.
     */
    private boolean isMulop() {
        switch(lookahead.getType()){
            case MULTIPLY:
                return true;
            case DIVIDE:
                return true;
            case MOD:
                return true;
            case AND:
                return true;
            default:
                return false;
        }
    }
    
    /**
     * Get the TokenType of the current token of rule Type.
     * @return the token type, null if none
     */
    private boolean isType() {
        switch(lookahead.getType()){
            case VOID:
                return true;
            case INT:
                return true;
            case FLOAT:
                return true;
            default:
                return false;
        }
    }
    
    /**
     * This checks whether to go down the term path.
     * Since it goes straight to matching a factor, this checks if the current token is a factor.
     * @return whether the lookahead token is a factor or not.
     */
    private boolean isTerm() {
        switch (lookahead.getType()) {
            case ID:
                return true;
            case NUMBER:
                return true;
            case LEFTPARENTHESES:
                return true;
            case NOT:
                return true;
            default:
                return false;
        }
    }
    
    /**
     * This checks whether to go down the statement_list path.
     * Since it goes straight to matching a statement, this checks if the current token is a statement.
     * @return whether the lookahead token is a statement or not.
     */
    private boolean isStatementList() {
        switch (lookahead.getType()) {
            case ID:
                return true;
            case LEFTCURLYBRACKET:
                return true;
            case IF:
                return true;
            case WHILE:
                return true;
            case READ:
                return true;
            case RETURN:
                return true;
            case WRITE:
                return true;
            default:
                return false;
        }
    }
    
    /*------------------------------------------------
                Non Helper Functions Below
    ------------------------------------------------*/
    
    /**
     * Check for a function declarations, "void main()", compound statement, and function definitions.
     */
    public void program() {
        function_declarations();
        match(TokenType.MAIN);
        match(TokenType.LEFTPARENTHESES);
        match(TokenType.RIGHTPARENTHESES);
        compound_statement();
        function_definitions();
        // check the lookahead token holds end of file
        match(TokenType.ENDOFFILE);
    }
    
    public void identifier_list() {
        tokenLexeme = lookahead.getLexeme();
        match(TokenType.ID);
        symbolTable.add(tokenLexeme, SymbolKind.VARIABLE_NAME);
        if (lookahead.getType() == TokenType.COMMA) {
            match(TokenType.COMMA);
            identifier_list();
        } else {
            // nothing
        }
    }
    
    public void declarations() {
        if (isType()) {
            type();
            identifier_list();
            match(TokenType.SEMICOLON);
            declarations();
        } else {
            // lambda
        }
    }
    
    public void function_declarations() {
        if (isType()) {
            function_declaration();
            match(TokenType.SEMICOLON);
            function_declarations();
        } else {
            // lambda
        }
    }
    
    public void function_declaration() {
        type();
        tokenLexeme = lookahead.getLexeme();
        match(TokenType.ID);
        symbolTable.add(tokenLexeme, SymbolKind.FUNCTION_NAME);
        parameters();
    }
    
    public void function_definitions() {
        if (isType()) {
            function_definition();
            function_definitions();
        } else {
            // lambda
        }
    }
    
    public void function_definition() {
        type();
        match(TokenType.ID);
        parameters();
        compound_statement();
    }
    
    public void parameters() {
        match(TokenType.LEFTPARENTHESES);
        parameter_list();
        match(TokenType.RIGHTPARENTHESES);
    }
    
    public void parameter_list() {
        if (isType()) {
            type();
            tokenLexeme = lookahead.getLexeme();
            match(TokenType.ID);
            symbolTable.add(tokenLexeme, SymbolKind.VARIABLE_NAME);
            if (lookahead.getType() == TokenType.COMMA) {
                match(TokenType.COMMA);
                parameter_list();
            } else  {
                // nothing
            }
        } else {
            // lambda
        }
    }
    
    public void compound_statement() {
        match(TokenType.LEFTCURLYBRACKET);
        declarations();
        optional_statements();
        match(TokenType.RIGHTCURLYBRACKET);
    }
    
    public void optional_statements() {
        if (isStatementList()) {
            statement_list();
        } else {
            // lambda
        }
    }
    
    public void statement_list() {
        statement();
        match(TokenType.SEMICOLON);
        if (isStatementList()) {
            statement_list();
        } else {
            // nothing
        }
    }
    
    public void statement() {
        switch(lookahead.getType()){
            case ID:
                if (symbolTable.getKind(lookahead.getLexeme()) == SymbolKind.VARIABLE_NAME) {
                    variable();
                    match(TokenType.ASSIGN);
                    expression();
                } else if (symbolTable.getKind(lookahead.getLexeme()) == SymbolKind.FUNCTION_NAME) {
                    procedure_statement();
                }
                break;
            case LEFTCURLYBRACKET:
                compound_statement();
                break;
            case IF:
                match(TokenType.IF);
                expression();
                match(TokenType.THEN);
                statement();
                match(TokenType.ELSE);
                statement();
                break;
            case WHILE:
                match(TokenType.WHILE);
                expression();
                match(TokenType.DO);
                statement();
                break;
            case READ:
                match(TokenType.READ);
                match(TokenType.LEFTPARENTHESES);
                match(TokenType.ID);
                match(TokenType.RIGHTPARENTHESES);
                break;
            case WRITE:
                match(TokenType.WRITE);
                match(TokenType.LEFTPARENTHESES);
                expression();
                match(TokenType.RIGHTPARENTHESES);
                break;
            case RETURN:
                match(TokenType.RETURN);
                expression();
                break;
            default:
                error("No match found in statement. Saw: " +  lookahead.getType());
        }
    }
    
    public void variable() {
        match(TokenType.ID);
        if (lookahead.getType() == TokenType.LEFTBRACKET)  {
            match(TokenType.LEFTBRACKET);
            expression();
            match(TokenType.RIGHTBRACKET);
        } else {
            // nothing
        }
    }
    
    public void procedure_statement() {
        match(TokenType.ID);
        if (lookahead.getType() == TokenType.LEFTPARENTHESES)  {
            match(TokenType.LEFTPARENTHESES);
            expression_list();
            match(TokenType.LEFTPARENTHESES);
        } else {
            //nothing
        }
    }
    
    public void expression_list() {
        expression();
        if (lookahead.getType() == TokenType.COMMA) {
            match(TokenType.COMMA);
            expression_list();
        } else {
            // nothing
        }
    }
    
    public void expression() {
        simple_expression();
        if (isRelop()) {
            relop();
            simple_expression();
        } else {
            // nothing
        }
    }
    
    public void simple_expression() {
        if (isTerm()) {
            term();
            simple_part();
        } else {
            sign();
            term();
            simple_part();
        }
    }
    
    public void simple_part() {
        if (isAddop()) {
            addop();
            term();
            simple_part();
        } else {
            // lambda
        }
    }
    
    public void term() {
        factor();
        term_part();
    }
    
    public void term_part() {
        if (isMulop()) {
            mulop();
            factor();
            term_part();
        } else {
            // lambda
        }
    }
    
    public void factor() {
        switch (lookahead.getType()) {
            case ID:
                match(TokenType.ID);
                if (lookahead.getType() == TokenType.LEFTBRACKET) {
                    match(TokenType.LEFTBRACKET);
                    expression();
                    match(TokenType.RIGHTBRACKET);
                } else if (lookahead.getType() == TokenType.LEFTPARENTHESES) {
                    match(TokenType.LEFTPARENTHESES);
                    expression_list();
                    match(TokenType.RIGHTPARENTHESES);
                } else {
                    // nothing
                }
                break;
            case NUMBER:
                match(TokenType.NUMBER);
                break;
            case LEFTPARENTHESES:
                match(TokenType.LEFTPARENTHESES);
                expression();
                match(TokenType.RIGHTPARENTHESES);
                break;
            case NOT:
                match(TokenType.NOT);
                factor();
                break;
            default:
                error("No match found in factor. Saw: " +  lookahead.getType());
        }
    }
    
    public void sign() {
        if (lookahead.getType() == TokenType.PLUS) {
            match(TokenType.PLUS);
        } else {
            match(TokenType.MINUS);
        }
    }
    
    /*------------------------------------------------
          Non Helper Relop, Mulop, Addop and Type
    ------------------------------------------------*/
    
    public void relop() {
        switch(lookahead.getType()){
            case EQUALS:
                match(TokenType.EQUALS);
                break;
            case NOTEQUAL:
                match(TokenType.NOTEQUAL);
                break;
            case LESSTHAN:
                match(TokenType.LESSTHAN);
                break;
            case GREATERTHAN:
                match(TokenType.GREATERTHAN);
                break;
            case LESSTHANOREQUAL:
                match(TokenType.LESSTHANOREQUAL);
                break;
            case GREATERTHANOREQUAL:
                match(TokenType.GREATERTHANOREQUAL);
                break;
        }
    }
    
    public void addop() {
        switch(lookahead.getType()){
            case PLUS:
                match(TokenType.PLUS);
                break;
            case MINUS:
                 match(TokenType.MINUS);
                 break;
            case OR:
                match(TokenType.OR);
                break;
        }
    }
    
    public void mulop() {
        switch(lookahead.getType()){
            case MULTIPLY:
                match(TokenType.MULTIPLY);
                break;
            case DIVIDE:
                match(TokenType.DIVIDE);
                break;
            case MOD:
                match(TokenType.MOD);
                break;
            case AND:
                match(TokenType.AND);
                break;
        }
    }
    
    public void type() {
        switch(lookahead.getType()){
            case VOID:
                match(TokenType.VOID);
                break;
            case INT:
                match(TokenType.INT);
                break;
            case FLOAT:
                match(TokenType.FLOAT);
                break;
        }
    }
}
