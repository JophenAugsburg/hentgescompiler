// Recognizer.java
// Joseph Hentges

package parser;

import syntaxTree.StatementNodes.*;
import syntaxTree.ExpressionNodes.*;
import util.TokenType;
import java.io.FileReader;
import java.io.StringReader;
import java.util.ArrayList;
import scanner.*;
import util.SymbolKind;
import symbolTable.SymbolTable;
import syntaxTree.*;
import util.DataType;

/**
 * This is the Recognizer class.
 * The start of the Recursive Descent Parser.
 * 
 * @author Joseph Hentges
 * January 21, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class Parser {
    
    Token lookahead;
    Scanner scanner;
    SymbolTable symbolTable;
    
    // helper global declaration of  the token lexeme
    String tokenLexeme;
    
    /**
     * This is the constructor.
     * 
     * @param stringFile : String - the String to be read, or a file path
     * @param isFile : boolean - whether the string being passed in is a file path or not
     */
    public Parser(String stringFile, boolean isFile) {
        if (isFile) {
            try {
                scanner = new Scanner(new FileReader(stringFile));
                symbolTable = new SymbolTable(stringFile);
            } catch(Exception error) {
                error.printStackTrace();
            }
        } else {
            scanner = new Scanner(new StringReader(stringFile));
            symbolTable = new SymbolTable("NOT_FILE");
        }
        
        try {
            lookahead = scanner.nextToken();
        } catch(Exception error) {
            error.printStackTrace();
        }
    }
    
    public SymbolTable getSymbolTable() {
        return symbolTable;
    }
    
    /**
     * Check if the current token has the same type as the expected type.
     * @param expected  : TokenType - the expected token type.
     */
    private void match(TokenType expected) {
        if (lookahead.getType() == expected) {
            try {
                lookahead = scanner.nextToken();
                if (lookahead == null) {
                    lookahead = new Token(TokenType.ENDOFFILE, "End of File");
                }
            } catch(Exception error) {
                error("Scanner exception");
            }
        } else {
            error("Match of " + expected + " found " + lookahead.getType() + " instead.");
        }
    }
    
    /**
     * Print an error out.
     * @param errorString : String - the error to be sent out.
     */
    private void error(String errorString) {
        throw new Error("Error: " + errorString);
    }
    
    /**
     * Determine if the lookahead type  is one of the following:
     * ==, !=, <, >, <=, >=
     * @return the type of the lookahead tokena, null if none.
     */
    private boolean isRelop() {
        switch(lookahead.getType()){
            case EQUALS:
                return true;
            case NOTEQUAL:
                return true;
            case LESSTHAN:
                return true;
            case GREATERTHAN:
                return true;
            case LESSTHANOREQUAL:
                return true;
            case GREATERTHANOREQUAL:
                return true;
            default:
                return false;
        }
    }
    
    private TokenType getRelop() {
        switch(lookahead.getType()){
            case EQUALS:
                return TokenType.EQUALS;
            case NOTEQUAL:
                return TokenType.NOTEQUAL;
            case LESSTHAN:
                return TokenType.LESSTHAN;
            case GREATERTHAN:
                return TokenType.GREATERTHAN;
            case LESSTHANOREQUAL:
                return TokenType.LESSTHANOREQUAL;
            case GREATERTHANOREQUAL:
                return TokenType.GREATERTHANOREQUAL;
            default:
                return null;
        }
    }
    
    /**
     * Determine if the lookahead type  is one of the following:
     * +, -, ||
     * @return whether the lookahead token type is one of those.
     */
    private boolean isAddop() {
        switch(lookahead.getType()){
            case PLUS:
                return true;
            case MINUS:
                return true;
            case OR:
                return true;
            default:
                return false;
        }
    }
    
    private TokenType getAddop() {
        switch(lookahead.getType()){
            case PLUS:
                return TokenType.PLUS;
            case MINUS:
                return TokenType.MINUS;
            case OR:
                return TokenType.OR;
            default:
                return null;
        }
    }
    
    /**
     * Determine if the lookahead type  is one of the following:
     * *, /, %, &&
     * @return whether the lookahead token type is one of those.
     */
    private boolean isMulop() {
        switch(lookahead.getType()){
            case MULTIPLY:
                return true;
            case DIVIDE:
                return true;
            case MOD:
                return true;
            case AND:
                return true;
            default:
                return false;
        }
    }
    
    /**
     * Get the TokenType of the current token of rule Type.
     * @return the token type, null if none
     */
    private boolean isType() {
        switch(lookahead.getType()){
            case VOID:
                return true;
            case INT:
                return true;
            case FLOAT:
                return true;
            default:
                return false;
        }
    }
    
    /**
     * This checks whether to go down the term path.
     * Since it goes straight to matching a factor, this checks if the current token is a factor.
     * @return whether the lookahead token is a factor or not.
     */
    private boolean isTerm() {
        switch (lookahead.getType()) {
            case ID:
                return true;
            case NUMBER:
                return true;
            case LEFTPARENTHESES:
                return true;
            case NOT:
                return true;
            default:
                return false;
        }
    }
    
    /**
     * This checks whether to go down the statement_list path.
     * Since it goes straight to matching a statement, this checks if the current token is a statement.
     * @return whether the lookahead token is a statement or not.
     */
    private boolean isStatementList() {
        switch (lookahead.getType()) {
            case ID:
                return true;
            case LEFTCURLYBRACKET:
                return true;
            case IF:
                return true;
            case WHILE:
                return true;
            case READ:
                return true;
            case RETURN:
                return true;
            case WRITE:
                return true;
            default:
                return false;
        }
    }
    
    private boolean isSign() {
        if (lookahead.getType() == TokenType.PLUS) {
            return true;
        } else if (lookahead.getType() == TokenType.MINUS) {
            return true;
        } else {
            return false;
        }
    }
    
    /*------------------------------------------------
                Non Helper Functions Below
    ------------------------------------------------*/
    
    /**
     * Check for a function declarations, "void main()", compound statement, and function definitions.
     */
    public ProgramNode program() {
        ProgramNode node = new ProgramNode();
        function_declarations();
        match(TokenType.MAIN);
        match(TokenType.LEFTPARENTHESES);
        match(TokenType.RIGHTPARENTHESES);
        node.setMain(compound_statement());
        FunctionsNode funcNode = new FunctionsNode();
        function_definitions(funcNode);
        node.setFunctions(funcNode);
        // check the lookahead token holds end of file
        match(TokenType.ENDOFFILE);
        return node;
    }
    
    public void identifier_list(DeclarationsNode node, DataType type) {
        tokenLexeme = lookahead.getLexeme();
        match(TokenType.ID);
        symbolTable.add(tokenLexeme, SymbolKind.VARIABLE_NAME);
        VariableNode varNode = new VariableNode(tokenLexeme);
        varNode.setType(type);
        node.addVariable(varNode);
        if (lookahead.getType() == TokenType.COMMA) {
            match(TokenType.COMMA);
            identifier_list(node, type);
        } else {
            // nothing
        }
    }
    
    public DeclarationsNode declarations(DeclarationsNode node) {
        if (isType()) {
            DataType dataType = type();
            identifier_list(node, dataType);
            match(TokenType.SEMICOLON);
            declarations(node);
        } else {
            // lambda
        }
        return node;
    }
    
    public void function_declarations() {
        if (isType()) {
            function_declaration();
            match(TokenType.SEMICOLON);
            function_declarations();
        } else {
            // lambda
        }
    }
    
    public void function_declaration() {
        type();
        tokenLexeme = lookahead.getLexeme();
        match(TokenType.ID);
        symbolTable.add(tokenLexeme, SymbolKind.FUNCTION_NAME);
        FunctionNode node = new FunctionNode(tokenLexeme);
        parameters(node, false);
    }
    
    public void function_definitions(FunctionsNode node) {
        if (isType()) {
            node.addFunctionDefinition(function_definition());
            function_definitions(node);
        } else {
            // lambda
        }
    }
    
    public FunctionNode function_definition() {
        DataType returnType = type();
        String lexeme = lookahead.getLexeme();
        match(TokenType.ID);
        FunctionNode node = new FunctionNode(lexeme);
        node.setReturnType(returnType);
        parameters(node, true);
        node.setBody(compound_statement());
        return node;
    }
    
    public void parameters(FunctionNode node, boolean addToTable) {
        match(TokenType.LEFTPARENTHESES);
        parameter_list(node, addToTable);
        match(TokenType.RIGHTPARENTHESES);
    }
    
    /**
     * Add the create and add the function node to the syntax tree
     * If not a function declaration, add the parameter declarations to the symbol table.
     * @param node the function node to add to
     * @param addToTable whether to add the parameter declarations to the symbol table
     */
    public void parameter_list(FunctionNode node, boolean addToTable) {
        if (isType()) {
            DataType dataType = type();
            tokenLexeme = lookahead.getLexeme();
            match(TokenType.ID);
            if (addToTable) {
                symbolTable.add(tokenLexeme, SymbolKind.VARIABLE_NAME);
            }
            VariableNode varNode = new VariableNode(tokenLexeme);
            varNode.setType(dataType);
            node.addParameter(varNode);
            if (lookahead.getType() == TokenType.COMMA) {
                match(TokenType.COMMA);
                parameter_list(node, addToTable);
            } else  {
                // nothing
            }
        } else {
            // lambda
        }
    }
    
    public CompoundStatementNode compound_statement() {
        CompoundStatementNode node = new CompoundStatementNode();
        match(TokenType.LEFTCURLYBRACKET);
        DeclarationsNode decNode = new DeclarationsNode();
        node.setVariables(declarations(decNode));
        optional_statements(node);
        match(TokenType.RIGHTCURLYBRACKET);
        return node;
    }
    
    public void optional_statements(CompoundStatementNode node) {
        if (isStatementList()) {
            statement_list(node);
        } else {
            // lambda
        }
    }
    
    public void statement_list(CompoundStatementNode node) {
        node.addStatement(statement());
        match(TokenType.SEMICOLON);
        if (isStatementList()) {
            statement_list(node);
        } else {
            // nothing
        }
    }
    
    public StatementNode statement() {
        StatementNode node = null;
        String lexeme;
        switch(lookahead.getType()){
            case ID:
                if (symbolTable.getKind(lookahead.getLexeme()) == SymbolKind.VARIABLE_NAME) {
                    AssignmentStatementNode assignNode = new AssignmentStatementNode();
                    assignNode.setLvalue(variable());
                    match(TokenType.ASSIGN);
                    assignNode.setExpression(expression());
                    node = assignNode;
                } else if (symbolTable.getKind(lookahead.getLexeme()) == SymbolKind.FUNCTION_NAME) {
                    node = procedure_statement();
                }
                break;
            case LEFTCURLYBRACKET:
                node = compound_statement();
                break;
            case IF:
                IfStatementNode ifNode = new IfStatementNode();
                match(TokenType.IF);
                ifNode.setTest(expression());
                ifNode.setThenStatement(compound_statement());
                match(TokenType.ELSE);
                ifNode.setElseStatement(compound_statement());
                node = ifNode;
                break;
            case WHILE:
                WhileStatementNode whileNode = new WhileStatementNode();
                match(TokenType.WHILE);
                whileNode.setTest(expression());
                whileNode.setBody(compound_statement());
                node = whileNode;
                break;
            case READ:
                ReadStatementNode readNode = new ReadStatementNode();
                match(TokenType.READ);
                match(TokenType.LEFTPARENTHESES);
                lexeme = lookahead.getLexeme();
                match(TokenType.ID);
                readNode.setVariable(lexeme);
                match(TokenType.RIGHTPARENTHESES);
                node = readNode;
                break;
            case WRITE:
                WriteStatementNode writeNode = new WriteStatementNode();
                match(TokenType.WRITE);
                match(TokenType.LEFTPARENTHESES);
                writeNode.setBody(expression());
                match(TokenType.RIGHTPARENTHESES);
                node = writeNode;
                break;
            case RETURN:
                ReturnStatementNode returnNode = new ReturnStatementNode();
                match(TokenType.RETURN);
                returnNode.setReturnBody(expression());
                node = returnNode;
                break;
            default:
                error("No match found in statement. Saw: " +  lookahead.getType());
        }
        return node;
    }
    
    public ExpressionNode variable() {
        String lexeme = lookahead.getLexeme();
        match(TokenType.ID);
        VariableNode node = new VariableNode(lexeme);
        if (lookahead.getType() == TokenType.LEFTBRACKET)  {
            match(TokenType.LEFTBRACKET);
            ArrayNode arrayNode = new ArrayNode(lexeme);
            lexeme = lookahead.getLexeme();
            if (lookahead.getType() == TokenType.NUMBER) {
                match(TokenType.NUMBER);
                arrayNode.setIndex(new ValueNode(lexeme));
            } else {
                match(TokenType.ID);
                arrayNode.setIndex(new VariableNode(lexeme));
            }
            match(TokenType.RIGHTBRACKET);
            return arrayNode;
        } else {
            // nothing
        }
        return node;
    }
    
    public StatementNode procedure_statement() {
        String lexeme = lookahead.getLexeme();
        ProcedureStatementNode procedureStatementNode = new ProcedureStatementNode(lexeme);
        match(TokenType.ID);
        if (lookahead.getType() == TokenType.LEFTPARENTHESES)  {
            match(TokenType.LEFTPARENTHESES);
            ArrayList<ExpressionNode> expressions = new ArrayList<>();
            expression_list(expressions);
            procedureStatementNode.setParameters(expressions);
            match(TokenType.RIGHTPARENTHESES);
        } else {
            //nothing
        }
        return procedureStatementNode;
    }
    
    public void expression_list(ArrayList<ExpressionNode> expressionsList) {
        ExpressionNode expNode = expression();
        if (expNode != null) {
        expressionsList.add(expNode);
        }
        if (lookahead.getType() == TokenType.COMMA) {
            match(TokenType.COMMA);
            expression_list(expressionsList);
        } else {
            // nothing
        }
    }
    
    public ExpressionNode expression() {
        ExpressionNode node = simple_expression();
        if (isRelop()) {
            OperationNode opNode = relop();
            opNode.setLeft(node);
            opNode.setRight(simple_expression());
            return opNode;
        } else {
            return node;
        }
    }
    
    public ExpressionNode simple_expression() {
        if (isTerm()) {
            ExpressionNode left = term();
            return simple_part(left);
        } else if (isSign()) {
            sign();
            ExpressionNode left = term();
            return simple_part(left);
        }
        return null;
    }
    
    public ExpressionNode simple_part(ExpressionNode left) {
        if (isAddop()) {
            OperationNode opNode = addop();
            ExpressionNode right = term();
            opNode.setLeft(left);
            opNode.setRight(right);
            return simple_part(opNode);
        } else {
            return left;
        }
    }
    
    public ExpressionNode term() {
        ExpressionNode left = factor();
        return term_part(left);
    }
    
    public ExpressionNode term_part(ExpressionNode left) {
        if (isMulop()) {
            OperationNode opNode = mulop();
            opNode.setLeft(left);
            opNode.setRight(factor());
            return term_part(opNode);
        } else {
            return left;
        }
    }
    
    public ExpressionNode factor() {
        ExpressionNode node = null;
        String lexeme;
        switch (lookahead.getType()) {
            case ID:
                lexeme = lookahead.getLexeme();
                match(TokenType.ID);
                if (lookahead.getType() == TokenType.LEFTBRACKET) {
                    match(TokenType.LEFTBRACKET);
                    ArrayNode arrayNode = new ArrayNode(lexeme);
                    lexeme = lookahead.getLexeme();
                    if (lookahead.getType() == TokenType.NUMBER) {
                        match(TokenType.NUMBER);
                        arrayNode.setIndex(new ValueNode(lexeme));
                    } else {
                        match(TokenType.ID);
                        arrayNode.setIndex(new VariableNode(lexeme));
                    }
                    node = arrayNode;
                    match(TokenType.RIGHTBRACKET);
                } else if (lookahead.getType() == TokenType.LEFTPARENTHESES) {
                    match(TokenType.LEFTPARENTHESES);
                    ArrayList<ExpressionNode> expressions = new ArrayList<>();
                    FunctionCallNode functionCallNode = new FunctionCallNode(lexeme);
                    expression_list(expressions);
                    functionCallNode.setArguments(expressions);
                    node = functionCallNode;
                    match(TokenType.RIGHTPARENTHESES);
                } else {
                    node = new VariableNode(lexeme);
                }
                break;
            case NUMBER:
                lexeme = lookahead.getLexeme();
                match(TokenType.NUMBER);
                node = new ValueNode(lexeme);
                break;
            case LEFTPARENTHESES:
                match(TokenType.LEFTPARENTHESES);
                node = expression();
                match(TokenType.RIGHTPARENTHESES);
                break;
            case NOT:
                match(TokenType.NOT);
                node = new OperationNode(TokenType.NOT, null, factor());
                break;
            default:
                error("No match found in factor. Saw: " +  lookahead.getType());
        }
        return node;
    }
    
    public void sign() {
        if (lookahead.getType() == TokenType.PLUS) {
            match(TokenType.PLUS);
        } else {
            match(TokenType.MINUS);
        }
    }
    
    /*------------------------------------------------
          Non Helper Relop, Mulop, Addop and Type
    ------------------------------------------------*/
    
    public OperationNode relop() {
        switch(lookahead.getType()){
            case EQUALS:
                match(TokenType.EQUALS);
                return new OperationNode(TokenType.EQUALS);
            case NOTEQUAL:
                match(TokenType.NOTEQUAL);
                return new OperationNode(TokenType.NOTEQUAL);
            case LESSTHAN:
                match(TokenType.LESSTHAN);
                return new OperationNode(TokenType.LESSTHAN);
            case GREATERTHAN:
                match(TokenType.GREATERTHAN);
                return new OperationNode(TokenType.GREATERTHAN);
            case LESSTHANOREQUAL:
                match(TokenType.LESSTHANOREQUAL);
                return new OperationNode(TokenType.LESSTHANOREQUAL);
            case GREATERTHANOREQUAL:
                match(TokenType.GREATERTHANOREQUAL);
                return new OperationNode(TokenType.GREATERTHANOREQUAL);
            default:
                return null;
        }
    }
    
    public OperationNode addop() {
        switch(lookahead.getType()){
            case PLUS:
                match(TokenType.PLUS);
                return new OperationNode(TokenType.PLUS);
            case MINUS:
                 match(TokenType.MINUS);
                return new OperationNode(TokenType.MINUS);
            case OR:
                match(TokenType.OR);
                return new OperationNode(TokenType.OR);
            default:
                return null;
        }
    }
    
    public OperationNode mulop() {
        switch(lookahead.getType()){
            case MULTIPLY:
                match(TokenType.MULTIPLY);
                return new OperationNode(TokenType.MULTIPLY);
            case DIVIDE:
                match(TokenType.DIVIDE);
                return new OperationNode(TokenType.DIVIDE);
            case MOD:
                match(TokenType.MOD);
                return new OperationNode(TokenType.MOD);
            case AND:
                match(TokenType.AND);
                return new OperationNode(TokenType.AND);
            default:
                return null;
        }
    }
    
    public DataType type() {
        switch(lookahead.getType()){
            case VOID:
                match(TokenType.VOID);
                return DataType.VOID;
            case INT:
                match(TokenType.INT);
                return DataType.INT;
            case FLOAT:
                match(TokenType.FLOAT);
                return DataType.FLOAT;
            default:
                return null;
        }
    }
}
