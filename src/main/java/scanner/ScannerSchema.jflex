// ScannerSchema.flex
// Joseph Hentges

package scanner;

/**
 * This is the JFlex defininition for the scanner.
 * The schema that is used in JFlex for creating a custom scanner.
 *
 * @author Joseph Hentges
 * Janary 21, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

/* 
  Note: to create/build the scanner, from this folder, in the terminal,  run this  command:
  
  $ rm Scanner.java; java -jar jflex.jar ScannerSchema.jflex;
*/

/* Declarations */


%%

%class Scanner /* Names the produced java file */
%public
%function nextToken /* Renames the yylex() function */
%type Token  /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
%line
%throws Exception
%{
  private LookupTable lookupTable = new LookupTable();
%}

/* Patterns */

other = .|#

AND = \&\&
OR = \|\|
NOTEQUAL = \!\=
LESSTHANOREQUAL = \<\=
GREATERTHANOREQUAL = \>\=
LEFTBRACKET  =  \[
RIGHTBRACKET = \]
EQUALS = \=\=
symbol = [\<\>\!\.\;\{\}\@\(\)\=\,\:\"\+\-\*/\%]|{OR}|{AND}|{NOTEQUAL}|{LESSTHANOREQUAL}|{GREATERTHANOREQUAL}|{LEFTBRACKET}|{RIGHTBRACKET}|{EQUALS}

letter = [A-Za-z]

digits = [1-9][0-9]*|0
number = {digits}+|{digits}+.{digits}+|{digits}+.{digits}+[e|E][+|-]?{digits}+

variableName = {letter}*|{digits}*
id = {letter}+{variableName}*

whitespace = [\s]+
stringContains = [ \t]*{letter}*|[ \t]*{number}*|[ \t]*{symbol}*
comment = [/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]|[/][/]{stringContains}*

%%

/* Lexical Rules */

{symbol} {
    if (lookupTable.containsKey(yytext())) {
        return new Token((TokenType) lookupTable.get(yytext()), yytext());
    }
}

{number} {
    return new Token(TokenType.NUMBER, yytext());
}

{id} {
    if (lookupTable.containsKey(yytext())) {
        return new Token((TokenType) lookupTable.get(yytext()), yytext());
    }
    return new Token(TokenType.ID, yytext());
}

{comment} {
  /* Ignore Comments */
}
            
{whitespace} {
  /* Ignore Whitespace */
}

{other} {
    throw new Exception("Illegal character detected! Character is: " + yytext() + " on line: " + (yyline+1));
}