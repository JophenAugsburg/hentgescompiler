// Token.java
// Joseph Hentges

package scanner;

import util.TokenType;

/**
 * This is the Token class.
 * 
 * @author Joseph Hentges
 * January 21, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public class Token 
{
    private static int IDCOUNT = 0;
    int ID;
    TokenType type;
    String lexeme;

    public Token(TokenType type, String lexeme) {
        ID = IDCOUNT;
        this.type = type;
        this.lexeme = lexeme;
        IDCOUNT += 1;
    }
    
    public TokenType getType() {
        return type;
    }
    
    public String getLexeme() {
        return lexeme;
    }

    @Override
    public String toString() {
        return "ID: " + ID + " | Type: " + type + " | Lexeme: " + lexeme;
    }

    /**
     * A basic equals function that tests whether two Tokens share the same type and lexeme.
     * @param compare
     * @return whether the two are equal
     */
    @Override
    public boolean equals(Object compare) {
        if (this.hashCode() == compare.hashCode()) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return type.hashCode() + lexeme.hashCode();
    }
}