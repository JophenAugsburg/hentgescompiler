package scanner;

import util.TokenType;
import java.util.HashMap;

/**
 * This is the LookupTable used by the scanner to find what type of 
 * token some text should be.
 * 
 * @author Joseph Hentges
 * January 21, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public class LookupTable extends HashMap {

    public LookupTable() {
        // Keywords
        this.put("char", TokenType.CHAR);
        this.put("int", TokenType.INT);
        this.put("float", TokenType.FLOAT);
        this.put("if", TokenType.IF);
        this.put("else", TokenType.ELSE);
        this.put("while", TokenType.WHILE);
        this.put("print", TokenType.PRINT);
        this.put("read", TokenType.READ);
        this.put("write", TokenType.WRITE);
        this.put("return", TokenType.RETURN);
        this.put("func", TokenType.FUNC);
        this.put("program", TokenType.PROGRAM);
        this.put("end", TokenType.END);
        this.put("void", TokenType.VOID);
        this.put("main", TokenType.MAIN);
        this.put("then", TokenType.THEN);
        this.put("do", TokenType.DO);

        // Symbols
        this.put(";", TokenType.SEMICOLON);
        this.put("(", TokenType.LEFTPARENTHESES);
        this.put(")", TokenType.RIGHTPARENTHESES);
        this.put("[", TokenType.LEFTBRACKET);
        this.put("]", TokenType.RIGHTBRACKET);
        this.put("{", TokenType.LEFTCURLYBRACKET);
        this.put("}", TokenType.RIGHTCURLYBRACKET);
        this.put("=", TokenType.ASSIGN);
        this.put("==", TokenType.EQUALS);
        this.put("+", TokenType.PLUS);
        this.put("-", TokenType.MINUS);
        this.put("*", TokenType.MULTIPLY);
        this.put("%", TokenType.MOD);
        this.put("/", TokenType.DIVIDE);
        this.put("<", TokenType.LESSTHAN);
        this.put(">", TokenType.GREATERTHAN);
        this.put("<=", TokenType.LESSTHANOREQUAL);
        this.put(">=", TokenType.GREATERTHANOREQUAL);
        this.put("!=", TokenType.NOTEQUAL);
        this.put("&&", TokenType.AND);
        this.put("||", TokenType.OR);
        this.put("!", TokenType.NOT);
        this.put(",", TokenType.COMMA);
    }
}