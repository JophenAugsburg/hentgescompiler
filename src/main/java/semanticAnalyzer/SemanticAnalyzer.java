// SemanticAnalyzer.java
// Joseph Hentges

package semanticAnalyzer;

import java.util.ArrayList;
import symbolTable.SymbolTable;
import syntaxTree.ExpressionNode;
import syntaxTree.ExpressionNodes.*;
import syntaxTree.FunctionNode;
import syntaxTree.FunctionsNode;
import syntaxTree.ProgramNode;
import syntaxTree.StatementNode;
import syntaxTree.StatementNodes.*;
import util.DataType;


/**
 * This is the Semantic Analyzer class.
 * 
 * @author Joseph Hentges
 * April 2, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class SemanticAnalyzer {
    
    private ProgramNode syntaxTree;
    private SymbolTable symbolTable;
    private boolean canWriteAssembly = true; // default TRUE - need to 'prove' you can't write assembly.
    
    private CompoundStatementNode main;
    private FunctionsNode functions;
    
    public SemanticAnalyzer(ProgramNode syntaxTree, SymbolTable symbolTable) {
        this.syntaxTree = syntaxTree;
        this.symbolTable = symbolTable;
    }
    
    public boolean canWriteAssembly() {
        return canWriteAssembly;
    }
    
    /**
     * Check to make sure everything defined was actually declared.
     */
    public void checkIdentifiersDeclarations() {
        CompoundStatementNode main = syntaxTree.getMain();
        FunctionsNode functions = syntaxTree.getFunctions();
        
        // check main
        ArrayList<VariableNode> declarations = main.getVariables().getVariables();
        for (StatementNode node : main.getStatements()) {
            if (node instanceof CompoundStatementNode) {
                declarations.addAll(((CompoundStatementNode) node).getVariables().getVariables());
            } else if (node instanceof WhileStatementNode) {
                declarations.addAll(((WhileStatementNode) node).getBody().getVariables().getVariables());
            } else if (node instanceof IfStatementNode) {
                declarations.addAll(((IfStatementNode) node).getThenStatement().getVariables().getVariables());
                declarations.addAll(((IfStatementNode) node).getElseStatement().getVariables().getVariables());
            }
        }
        for (String line : main.indentedToString(0).split("\n")) {
            if (line.contains("Name: ")) {
                String variableName = line.split("(: )| \\| ")[1];
                boolean contains = false;
                for (VariableNode dec : declarations) {
                    if (dec.getName().equals(variableName)) {
                        contains = true;
                    }
                }
                if (!contains) {
                    canWriteAssembly = false;
                }
            }
        }
        // check functions
        for (FunctionNode function : functions.getFunctions()) {
            // set up the declared variables
            ArrayList<VariableNode> functionDeclarations = function.getBody().getVariables().getVariables();
            // add the parameters to the functionDeclarations list
            functionDeclarations.addAll(function.getParameters());
            for (String line : function.getBody().indentedToString(0).split("\n")) {
                if (line.contains("Name: ")) {
                    String variableName = line.split("(: )| \\| ")[1];
                    boolean contains = false;
                    for (VariableNode dec : functionDeclarations) {
                        if (dec.getName().equals(variableName)) {
                            contains = true;
                        }
                    }
                    if (!contains) {
                        canWriteAssembly = false;
                    }
                }
            }
        }
    }
    
    /**
     * Assign Data Types to all of the Expression nodes.
     */
    public void assignDataTypes() {
        main = syntaxTree.getMain();
        functions = syntaxTree.getFunctions();
        
        // go through main function statements
        ArrayList<VariableNode> declarations = main.getVariables().getVariables();
        for (StatementNode node : main.getStatements()) {
            if (node instanceof AssignmentStatementNode) {
                DataType lValueType = getDataType(declarations, ((AssignmentStatementNode) node).getLvalue());
                ((AssignmentStatementNode) node).getLvalue().setType(lValueType);
                setExpressionDataType(declarations, ((AssignmentStatementNode) node).getExpression());
            } else if (node instanceof CompoundStatementNode) {
                assignCompoundStatementDataTypes(declarations, ((CompoundStatementNode) node));
            } else if (node instanceof IfStatementNode) {
                setExpressionDataType(declarations, ((IfStatementNode) node).getTest());
                assignCompoundStatementDataTypes(declarations, ((IfStatementNode) node).getThenStatement());
                assignCompoundStatementDataTypes(declarations, ((IfStatementNode) node).getElseStatement());
            } else if (node instanceof ProcedureStatementNode) {
                for (ExpressionNode procExpNode : ((ProcedureStatementNode) node).getParameters()) {
                    setExpressionDataType(declarations, procExpNode);
                }
            } else if (node instanceof ReturnStatementNode) {
                setExpressionDataType(declarations, ((ReturnStatementNode) node).getReturnBody());
            } else if (node instanceof WhileStatementNode) {
                setExpressionDataType(declarations, ((WhileStatementNode) node).getTest());
                assignCompoundStatementDataTypes(declarations, ((WhileStatementNode) node).getBody());
            } else if (node instanceof WriteStatementNode) {
                setExpressionDataType(declarations, ((WriteStatementNode) node).getBody());
            }
        }
        
        // go through the defined functions
        for (FunctionNode function : functions.getFunctions()) {
            ArrayList<VariableNode> functionDeclarations = function.getBody().getVariables().getVariables();
            functionDeclarations.addAll(function.getParameters());
            for (StatementNode node : function.getBody().getStatements()) {
                if (node instanceof AssignmentStatementNode) {
                    DataType lValueType = getDataType(functionDeclarations, ((AssignmentStatementNode) node).getLvalue());
                    ((AssignmentStatementNode) node).getLvalue().setType(lValueType);
                    setExpressionDataType(functionDeclarations, ((AssignmentStatementNode) node).getExpression());
                } else if (node instanceof CompoundStatementNode) {
                    assignCompoundStatementDataTypes(functionDeclarations, ((CompoundStatementNode) node));
                } else if (node instanceof IfStatementNode) {
                    setExpressionDataType(functionDeclarations, ((IfStatementNode) node).getTest());
                    assignCompoundStatementDataTypes(functionDeclarations, ((IfStatementNode) node).getThenStatement());
                    assignCompoundStatementDataTypes(functionDeclarations, ((IfStatementNode) node).getElseStatement());
                } else if (node instanceof ProcedureStatementNode) {
                    for (ExpressionNode procExpNode : ((ProcedureStatementNode) node).getParameters()) {
                        setExpressionDataType(functionDeclarations, procExpNode);
                    }
                } else if (node instanceof ReturnStatementNode) {
                    setExpressionDataType(functionDeclarations, ((ReturnStatementNode) node).getReturnBody());
                } else if (node instanceof WhileStatementNode) {
                    setExpressionDataType(functionDeclarations, ((WhileStatementNode) node).getTest());
                    assignCompoundStatementDataTypes(functionDeclarations, ((WhileStatementNode) node).getBody());
                } else if (node instanceof WriteStatementNode) {
                    setExpressionDataType(functionDeclarations, ((WriteStatementNode) node).getBody());
                }
            }
        }
    }
    
    /**
     * Check to make sure everything being assigned some expression matched the expression data type.
     * Only look at assignment statement nodes
     */
    public void checkAssignmentTypes() {
        CompoundStatementNode main = syntaxTree.getMain();
        FunctionsNode functions = syntaxTree.getFunctions();
        
        // go through main function statements
        ArrayList<VariableNode> declarations = main.getVariables().getVariables();
        for (StatementNode node : main.getStatements()) {
            if (node instanceof AssignmentStatementNode) {
                DataType lValueType = getDataType(declarations, ((AssignmentStatementNode) node).getLvalue());
                checkAssignmentExpressions(((AssignmentStatementNode) node).getExpression(), lValueType);
            }
        }
        
        // go through the defined functions
        for (FunctionNode function : functions.getFunctions()) {
            ArrayList<VariableNode> functionDeclarations = function.getBody().getVariables().getVariables();
            for (StatementNode node : function.getBody().getStatements()) {
                if (node instanceof AssignmentStatementNode) {
                    DataType lValueType = getDataType(functionDeclarations, ((AssignmentStatementNode) node).getLvalue());
                    checkAssignmentExpressions(((AssignmentStatementNode) node).getExpression(), lValueType);
                }
            }
        }
    }
    
    /*---------------------------------------
                Helper Fucntions
    ----------------------------------------*/
    
    /**
     * Essentially, repeat the code in assignDataTypes - but with a given CompoundStatementNode
     * @param compoundStatement - the compoundStatementNode to work off of
     */
    private void assignCompoundStatementDataTypes(ArrayList<VariableNode> declarations, CompoundStatementNode compoundStatement) {
        declarations.addAll(compoundStatement.getVariables().getVariables());
        for (StatementNode node : compoundStatement.getStatements()) {
            if (node instanceof AssignmentStatementNode) {
                DataType lValueType = getDataType(declarations, ((AssignmentStatementNode) node).getLvalue());
                ((AssignmentStatementNode) node).getLvalue().setType(lValueType);
                setExpressionDataType(declarations, ((AssignmentStatementNode) node).getExpression());
            } else if (node instanceof CompoundStatementNode) {
                assignCompoundStatementDataTypes(declarations, ((CompoundStatementNode) node));
            } else if (node instanceof IfStatementNode) {
                setExpressionDataType(declarations, ((IfStatementNode) node).getTest());
                assignCompoundStatementDataTypes(declarations, ((IfStatementNode) node).getThenStatement());
                assignCompoundStatementDataTypes(declarations, ((IfStatementNode) node).getElseStatement());
            } else if (node instanceof ProcedureStatementNode) {
                for (ExpressionNode procExpNode : ((ProcedureStatementNode) node).getParameters()) {
                    setExpressionDataType(declarations, procExpNode);
                }
            } else if (node instanceof ReturnStatementNode) {
                setExpressionDataType(declarations, ((ReturnStatementNode) node).getReturnBody());
            } else if (node instanceof WhileStatementNode) {
                setExpressionDataType(declarations, ((WhileStatementNode) node).getTest());
                assignCompoundStatementDataTypes(declarations, ((WhileStatementNode) node).getBody());
            } else if (node instanceof WriteStatementNode) {
                setExpressionDataType(declarations, ((WriteStatementNode) node).getBody());
            }
        }
    }
    
    /**
     * Assign data types to nodes in expressions
     * @param declarations - a list of declarations to use
     * @param expNode - the expression to work off of
     */
    private void setExpressionDataType(ArrayList<VariableNode> declarations, ExpressionNode expNode) {
        DataType dataType;
        if (expNode instanceof VariableNode) {
            dataType = getDataType(declarations, ((VariableNode) expNode));
            ((VariableNode) expNode).setType(dataType);
        } else if (expNode instanceof OperationNode) {
            setExpressionDataType(declarations, ((OperationNode) expNode).getLeft());
            setExpressionDataType(declarations, ((OperationNode) expNode).getRight());
        } else if (expNode instanceof FunctionCallNode) {
            dataType = getDataTypeOfFunction(((FunctionCallNode) expNode));
            ((FunctionCallNode) expNode).setType(dataType);
            for (ExpressionNode funcExpNode : ((FunctionCallNode) expNode).getArguments()) {
                setExpressionDataType(declarations, funcExpNode);
            }
        }
    }
    
    /**
     * Find the DataType of the given VariableNode
     * @param declarations : the variable node declarations - list of data types for each VariableNode
     * @param variable : the variable node to find the data type of
     * @return the data type
     */
    private DataType getDataType(ArrayList<VariableNode> declarations, ExpressionNode variable) {
        for (VariableNode varNode : declarations) {
            if (variable instanceof VariableNode) {
                if (varNode.getName().equals(((VariableNode)variable).getName())) {
                    return varNode.getType();
                }
            } else if (variable instanceof ArrayNode) {
                if (varNode.getName().equals(((ArrayNode)variable).getName())) {
                    return varNode.getType();
                }
            }
        }
        canWriteAssembly = false;
        return null;
    }
    
    /**
     * Get the return type (data type) of a function call
     * @param node the function call node
     * @return the return type
     */
    private DataType getDataTypeOfFunction(FunctionCallNode node) {
        for (FunctionNode funcNode : functions.getFunctions()) {
            if (funcNode.getName().equals(node.getName())) {
                return funcNode.getReturnType();
            }
        }
        canWriteAssembly = false;
        return null;
    }
    
    /**
     * Pass along and check that all expressions in the expression node match data types
     * @param expNode - the expression node to check
     * @param dataType - the data type of the expression node statement
     */
    private void checkAssignmentExpressions(ExpressionNode expNode, DataType lValueDataType) {
        if (expNode instanceof VariableNode) {
            if (((VariableNode) expNode).getType() != lValueDataType) {
                canWriteAssembly = false;
            }
        } else if (expNode instanceof OperationNode) {
            checkAssignmentExpressions(((OperationNode) expNode).getLeft(), lValueDataType);
            checkAssignmentExpressions(((OperationNode) expNode).getRight(), lValueDataType);
        } else if (expNode instanceof FunctionCallNode) {
            if (((FunctionCallNode) expNode).getType() != lValueDataType) {
                canWriteAssembly = false;
            }
            for (ExpressionNode funcExpNode : ((FunctionCallNode) expNode).getArguments()) {
                checkAssignmentExpressions(funcExpNode, lValueDataType);
            }
        }
    }
}
