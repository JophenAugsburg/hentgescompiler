// CompilerMain.java
// Joseph Hentges

package compiler;

import codeGeneration.CodeGenerator;
import java.io.PrintWriter;
import parser.Parser;
import semanticAnalyzer.SemanticAnalyzer;
import symbolTable.SymbolTable;
import syntaxTree.ProgramNode;

/**
 * This is the Compiler's main file.
 * 
 * @author Joseph Hentges
 * April 9, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public class CompilerMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Parser parser = new Parser(args[0], true);
        
        try {
            ProgramNode syntaxTree = parser.program();
            SymbolTable symbolTable = parser.getSymbolTable();
            SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
            
            // Create and write to symbols and tree files.
            try {
                PrintWriter writer = new PrintWriter(symbolTable.getSymbolFileName(), "UTF-8");
                writer.println(symbolTable.toString());
                writer.close();
                
                writer = new PrintWriter(symbolTable.getSymbolFileName().replaceFirst("[.][^.]+$", "") + ".tree", "UTF-8");
                writer.println("The Syntax Tree For " + symbolTable.getSymbolFileName().replaceFirst("[.][^.]+$", "") + ".\n");
                writer.println(syntaxTree.indentedToString(0));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            // Run the semantic analyzer.
            semanticAnalyzer.checkIdentifiersDeclarations();
            System.out.println("Checked Declarations: " + semanticAnalyzer.canWriteAssembly());
            semanticAnalyzer.assignDataTypes();
            System.out.println("Assigned Data Types: " + semanticAnalyzer.canWriteAssembly());
            semanticAnalyzer.checkAssignmentTypes();
            System.out.println("Checked Assignment Types: " + semanticAnalyzer.canWriteAssembly());
            
            // can write assembly?
            if (semanticAnalyzer.canWriteAssembly()) {
                System.out.println("Writing Assembly...");
                CodeGenerator codeGenerator = new CodeGenerator(syntaxTree, symbolTable);
                String assemblyCode = codeGenerator.generateCode();
                try {
                    PrintWriter writer = new PrintWriter(symbolTable.getSymbolFileName().replaceFirst("[.][^.]+$", "") + ".asm", "UTF-8");
                    writer.println(assemblyCode);
                    writer.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Can't write assembly");
            }
        } catch (Error e) {
            e.printStackTrace();
        }
    }
    
}
