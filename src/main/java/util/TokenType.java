// TokenType.java
// Joseph Hentges

package util;

/**
 * This is an enum of TokenTypes for the scanner.
 * It is  the possible types of tokens the scanner could read and generate.
 * 
 * @author Joseph Hentges
 * January 21, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public enum TokenType {
    /*
      keywords
    */
    CHAR, // char
    INT, // int
    FLOAT, // float
    IF, // if
    ELSE, // else
    WHILE, // while
    PRINT, // print
    READ, // read
    WRITE, // write
    RETURN, // return
    FUNC, // func
    PROGRAM, // program
    END, // end
    VOID, // void
    MAIN, // main
    THEN, // then
    DO, // do

    /*
      non-keywords
    */
    ID, // any set of characters & numbers starting with a letter

    /*
      numbers
    */
    NUMBER, // any number int, float, scientific notation...

    /*
      symbols
    */
    SEMICOLON, // ;
    LEFTPARENTHESES, // (
    RIGHTPARENTHESES, // )
    LEFTCURLYBRACKET, // {
    RIGHTCURLYBRACKET, // }
    LEFTBRACKET, // [
    RIGHTBRACKET, // ]
    ASSIGN, // =
    EQUALS, // ==
    PLUS, // +
    MINUS, // -
    MULTIPLY, // *
    MOD, // %
    DIVIDE, // \
    LESSTHAN, // <
    GREATERTHAN, // >
    LESSTHANOREQUAL, // <=
    GREATERTHANOREQUAL, // >=
    NOTEQUAL, // !=
    AND, // &&
    OR, // ||
    NOT, // !
    COMMA, // ,
    
    // This is used purely for "nullPointerException" in the recognizer.
    // Uses this TokenType since the scanner will never match to it.
    ENDOFFILE,
}