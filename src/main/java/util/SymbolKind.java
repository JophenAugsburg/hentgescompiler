// SymbolKind.java
// Joseph Hentges

package util;

/**
 * This is an enum of SymbolKinds for the Symbol Table.
 * 
 * @author Joseph Hentges
 * February 17, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public enum SymbolKind {
    VARIABLE_NAME,
    FUNCTION_NAME
}
