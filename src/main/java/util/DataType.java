// DataType.java
// Joseph Hentges

package util;

/**
 * This is an enum of DataTypes for the Symbol Table.
 * 
 * @author Joseph Hentges
 * February 27, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public enum DataType {
    VOID,
    INT,
    FLOAT
}
