// SyntaxTeeeTest.java
// Joseph Hentges

package syntaxTree;

import syntaxTree.StatementNodes.AssignmentStatementNode;
import syntaxTree.ExpressionNodes.VariableNode;
import syntaxTree.ExpressionNodes.ValueNode;
import syntaxTree.ExpressionNodes.OperationNode;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import util.TokenType;
import parser.Parser;

/**
 * These are the Syntax Tree jUnit tests.
 * 
 * @author Joseph Hentges
 * March 15, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public class SyntaxTreeTest {
    
     /**
     * Test of indentedToString method, of class SyntaxTreeNode.
     */
    @Test
    public void testIndentedToString() {
        System.out.println("indentedToString - 3 * 4.7");
        int level = 0;
        OperationNode actual = new OperationNode( TokenType.MULTIPLY);
        ValueNode left = new ValueNode( "3");
        ValueNode right = new ValueNode( "4.7");
        actual.setLeft(left);
        actual.setRight(right);
        String expected = "Operation: MULTIPLY\n" +
                          "|-- Value: 3\n" +
                          "|-- Value: 4.7\n";
        String actualString = actual.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /**
     * Test of indentedToString method, of class SyntaxTreeNode.
     */
    @Test
    public void testIndentedToString_two() {
        System.out.println("indentedToString - apple = 1 + 2 + 3");
        int level = 0;
        AssignmentStatementNode actual = new AssignmentStatementNode();
        VariableNode left = new VariableNode("apple");
        OperationNode right = new OperationNode(TokenType.PLUS);
        ValueNode leftOp = new ValueNode("1");
        OperationNode rightOp = new OperationNode(TokenType.PLUS);
        ValueNode leftOpOp = new ValueNode("2");
        ValueNode rightOpOp = new ValueNode("3");
        right.setLeft(leftOp);
        rightOp.setLeft(leftOpOp);
        rightOp.setRight(rightOpOp);
        right.setRight(rightOp);
        actual.setLvalue(left);
        actual.setExpression(right);
        String expected = "Assignment\n" +
                    "|-- Name: apple | Datatype: null\n" +
                    "|-- Operation: PLUS\n" +
                    "|-- --- Value: 1\n" +
                    "|-- --- Operation: PLUS\n" +
                    "|-- --- --- Value: 2\n" +
                    "|-- --- --- Value: 3\n";
        String actualString = actual.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
}
