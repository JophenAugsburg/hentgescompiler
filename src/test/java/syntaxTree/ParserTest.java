// ParserTest.java
// Joseph Hentges

package syntaxTree;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import parser.Parser;
import util.SymbolKind;

/**
 * These are the Syntax Tree from the Parser jUnit tests.
 * 
 * @author Joseph Hentges
 * March 15, 2020
 * April 2, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class ParserTest {
    
    /*-----------------------------
            Factor Tests
    ------------------------------*/
    
    /**
     * Test of the Parser Factor function with ! 17
     */
    @Test
    public void testFactor_One() {
        System.out.println("factor - ! 17");
        Parser instance = new Parser("! 17", false);
        ExpressionNode node = instance.factor();
        String expected = "Operation: NOT\n" +
                        "|-- Value: 17\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /**
     * Test of the Parser Factor function with (17 + 3)
     */
    @Test
    public void testFactor_Two() {
        System.out.println("factor - (17 + 3)");
        Parser instance = new Parser("(17 + 3)", false);
        ExpressionNode node = instance.factor();
        String expected = "Operation: PLUS\n" +
                        "|-- Value: 17\n" +
                        "|-- Value: 3\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /**
     * Test of the Parser Factor function with temp(15, 16)
     */
    @Test
    public void testFactor_Three() {
        System.out.println("factor - temp(15, 16)");
        Parser instance = new Parser("temp(15, 16)", false);
        ExpressionNode node = instance.factor();
        String expected = "Function: temp | Returns: null\n" +
                        "|-- Value: 15\n" +
                        "|-- Value: 16\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /*-----------------------------
         Simple Expression Tests
    ------------------------------*/
    
    /**
     * Test of the Parser Simple_Expression function with ! 17
     */
    @Test
    public void testSimpleExplression_One() {
        System.out.println("simple_expression - apple(13, 12) + 5 + 3 *4");
        Parser instance = new Parser("apple(13, 12) + 5 + 3 *4", false);
        ExpressionNode node = instance.simple_expression();
        String expected = "Operation: PLUS\n" +
                    "|-- Operation: PLUS\n" +
                    "|-- --- Function: apple | Returns: null\n" +
                    "|-- --- --- Value: 13\n" +
                    "|-- --- --- Value: 12\n" +
                    "|-- --- Value: 5\n" +
                    "|-- Operation: MULTIPLY\n" +
                    "|-- --- Value: 3\n" +
                    "|-- --- Value: 4\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /**
     * Test of the Parser Simple_Expression function with ! 17
     */
    @Test
    public void testSimpleExplression_Two() {
        System.out.println("simple_expression - 15 * 3 - 4 + 3 * apple()");
        Parser instance = new Parser("15 * 3 - 4 + 3 * apple()", false);
        ExpressionNode node = instance.simple_expression();
        String expected = "Operation: PLUS\n" +
                "|-- Operation: MINUS\n" +
                "|-- --- Operation: MULTIPLY\n" +
                "|-- --- --- Value: 15\n" +
                "|-- --- --- Value: 3\n" +
                "|-- --- Value: 4\n" +
                "|-- Operation: MULTIPLY\n" +
                "|-- --- Value: 3\n" +
                "|-- --- Function: apple | Returns: null\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /*-----------------------------
            Statement Tests
    ------------------------------*/
    
    /**
     * Test of the Parser Statement function with apple = 10 + 2
     */
    @Test
    public void testStatement_One() {
        System.out.println("statement - apple = 10 + 2");
        Parser instance = new Parser("apple = 10 + 2", false);
        instance.getSymbolTable().add("apple", SymbolKind.VARIABLE_NAME);
        StatementNode node = instance.statement();
        String expected = "Assignment\n" +
                "|-- Name: apple | Datatype: null\n" +
                "|-- Operation: PLUS\n" +
                "|-- --- Value: 10\n" +
                "|-- --- Value: 2\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /**
     * Test of the Parser Statement function with { int apple; if 5 < 2 apple = 4 else apple = 5; }
     */
    @Test
    public void testStatement_Two() {
        System.out.println("statement - { int apple; if 5 < 2 { apple = 4 } else { apple = 5; } }");
        Parser instance = new Parser("{ int apple; if 5 < 2 { apple = 4; } else { apple = 5; }; }", false);
        StatementNode node = instance.statement();
        String expected = "Compound Statement\n" +
                    "|-- Declarations\n" +
                    "|-- --- Name: apple | Datatype: INT\n" +
                    "|-- If\n" +
                    "|-- --- Operation: LESSTHAN\n" +
                    "|-- --- --- Value: 5\n" +
                    "|-- --- --- Value: 2\n" +
                    "|-- --- Then\n" +
                    "|-- --- --- Compound Statement\n" +
                    "|-- --- --- --- Declarations\n" +
                    "|-- --- --- --- Assignment\n" +
                    "|-- --- --- --- --- Name: apple | Datatype: null\n" +
                    "|-- --- --- --- --- Value: 4\n" +
                    "|-- --- Else\n" +
                    "|-- --- --- Compound Statement\n" +
                    "|-- --- --- --- Declarations\n" +
                    "|-- --- --- --- Assignment\n" +
                    "|-- --- --- --- --- Name: apple | Datatype: null\n" +
                    "|-- --- --- --- --- Value: 5\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /*-----------------------------
      Function Definition Tests
    ------------------------------*/
    
    /**
     * Test of the Parser Function Definition function with void apple() { int thisDog; thisDog = 15 + 2; }
     */
    @Test
    public void testFunctionDefinition_One() {
        System.out.println("function_definition - void apple() { int thisDog; thisDog = 15 + 2; }");
        Parser instance = new Parser("void apple() { int thisDog; thisDog = 15 + 2; }", false);
        FunctionNode node = instance.function_definition();
        String expected = "Function: apple returns VOID\n" +
                "|-- Compound Statement\n" +
                "|-- --- Declarations\n" +
                "|-- --- --- Name: thisDog | Datatype: INT\n" +
                "|-- --- Assignment\n" +
                "|-- --- --- Name: thisDog | Datatype: null\n" +
                "|-- --- --- Operation: PLUS\n" +
                "|-- --- --- --- Value: 15\n" +
                "|-- --- --- --- Value: 2\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /**
     * Test of the Parser Function Definition function with int apple() { int thisDog; thisDog = 15 + 2; return thisDog; }
     */
    @Test
    public void testFunctionDefinition_Two() {
        System.out.println("function_definition - int apple() { int thisDog; thisDog = 15 + 2; return thisDog; }");
        Parser instance = new Parser("int apple() { int thisDog; thisDog = 15 + 2; return thisDog; }", false);
        FunctionNode node = instance.function_definition();
        String expected = "Function: apple returns INT\n" +
                    "|-- Compound Statement\n" +
                    "|-- --- Declarations\n" +
                    "|-- --- --- Name: thisDog | Datatype: INT\n" +
                    "|-- --- Assignment\n" +
                    "|-- --- --- Name: thisDog | Datatype: null\n" +
                    "|-- --- --- Operation: PLUS\n" +
                    "|-- --- --- --- Value: 15\n" +
                    "|-- --- --- --- Value: 2\n" +
                    "|-- --- Return:\n" +
                    "|-- --- --- Name: thisDog | Datatype: null\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /*-----------------------------
          Declarations Tests
    ------------------------------*/
    
    /**
     * Test of the Parser Declarations function with int a, b, c; float de, f;
     */
    @Test
    public void testDeclarations_One() {
        System.out.println("declarations - int a, b, c; float de, f;");
        Parser instance = new Parser("int a, b, c; float de, f;", false);
        DeclarationsNode node = new DeclarationsNode();
        instance.declarations(node);
        String expected = "Declarations\n" +
                "|-- Name: a | Datatype: INT\n" +
                "|-- Name: b | Datatype: INT\n" +
                "|-- Name: c | Datatype: INT\n" +
                "|-- Name: de | Datatype: FLOAT\n" +
                "|-- Name: f | Datatype: FLOAT\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /**
     * Test of the Parser Declarations function with int a; int b; int c; float d, e, f;
     */
    @Test
    public void testDeclarations_Two() {
        System.out.println("declarations - int a; int b; int c; float d, e, f;");
        Parser instance = new Parser("int a; int b; int c; float d, e, f;", false);
        DeclarationsNode node = new DeclarationsNode();
        instance.declarations(node);
        String expected = "Declarations\n" +
                "|-- Name: a | Datatype: INT\n" +
                "|-- Name: b | Datatype: INT\n" +
                "|-- Name: c | Datatype: INT\n" +
                "|-- Name: d | Datatype: FLOAT\n" +
                "|-- Name: e | Datatype: FLOAT\n" +
                "|-- Name: f | Datatype: FLOAT\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /*-----------------------------
            Program Tests
    ------------------------------*/
    
    /**
     * Test of the Parser Program function with int func1(int a); void func2(float b); main(){int c;} int func3(int d){int s; s = 5;}
     */
    @Test
    public void testProgram_One() {
        System.out.println("program - int func1(int a); void func2(float b); main(){int c;} int func3(int d){int s; s = 5;}");
        Parser instance = new Parser("int func1(int a); void func2(float b); main(){int c;} int func3(int d){int s; s = 5;}", false);
        ProgramNode node = instance.program();
        String expected = "Program:\n" +
                    "|-- FunctionDefinitions\n" +
                    "|-- --- Function: func3 returns INT\n" +
                    "|-- --- --- Name: d | Datatype: INT\n" +
                    "|-- --- --- Compound Statement\n" +
                    "|-- --- --- --- Declarations\n" +
                    "|-- --- --- --- --- Name: s | Datatype: INT\n" +
                    "|-- --- --- --- Assignment\n" +
                    "|-- --- --- --- --- Name: s | Datatype: null\n" +
                    "|-- --- --- --- --- Value: 5\n" +
                    "|-- Compound Statement\n" +
                    "|-- --- Declarations\n" +
                    "|-- --- --- Name: c | Datatype: INT\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
    
    /**
     * Test of the Parser Program function with void func1(int a); int func2(void b); main(){int c;}
     */
    @Test
    public void testProgram_Two() {
        System.out.println("program - void func1(int a); int func2(void b); main(){int c;}");
        Parser instance = new Parser("void func1(int a); int func2(void b); main(){int c;}", false);
        ProgramNode node = instance.program();
        String expected = "Program:\n" +
                "|-- FunctionDefinitions\n" +
                "|-- Compound Statement\n" +
                "|-- --- Declarations\n" +
                "|-- --- --- Name: c | Datatype: INT\n";
        String actualString = node.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print( actualString);
        assertEquals( expected, actualString);
    }
}
