// CodeGeneratorTest.java
// Joseph Hentges

package codeGeneration;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import parser.Parser;
import semanticAnalyzer.SemanticAnalyzer;
import symbolTable.SymbolTable;
import syntaxTree.ExpressionNodes.*;
import syntaxTree.*;
import syntaxTree.StatementNodes.*;
import util.TokenType;

/**
 * These are the Code Generation Tests
 * 
 * @author Joseph Hentges
 * April 18, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class CodeGeneratorTest {

    /**
     * Test of writeCode method, of class CodeGenerator.
     */
    @Test
    public void testWriteCode_ValueNode_String() {
        System.out.println("checkIdentifiersDeclarations -");
        Parser parser = new Parser("main(){int c; c = 4; c = c + 2;}", false);
        ProgramNode syntaxTree = parser.program();
        SymbolTable symbolTable = parser.getSymbolTable();
        SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
        semanticAnalyzer.checkIdentifiersDeclarations();
        semanticAnalyzer.assignDataTypes();
        semanticAnalyzer.checkAssignmentTypes();
        CodeGenerator codeGen = new CodeGenerator(syntaxTree, symbolTable);
        String actual = codeGen.generateCode();
        //System.out.println("The actual result is:\n" + actual);
        String expected = ".data\n" +
                "  c :   .word   0\n" +
                "\n\n.text\n\n" +
                "main:\n" +
                "  addi   $sp, $sp, -4\n" +
                "  sw     $ra, 0($sp)\n" +
                "  li     $t0, 4\n" +
                "  sw     $t0, c\n" +
                "  lw     $t1, c\n" +
                "  li     $t2, 2\n" +
                "  add    $t0, $t1, $t2\n" +
                "  sw     $t0, c\n" +
                "  lw     $ra 0($sp)\n" +
                "  addi   $sp, $sp, 4\n" +
                "  jr     $ra";
        //assertEquals( expected, actual);
    }
    
    /**
     * Test of writeCode method, of class CodeGenerator.
     */
    @Test
    public void testWriteCode_Two() {
        System.out.println("generate code - main() { int aa, bb; aa = 10 ; if aa < 15 { bb = 6; write(bb+2); } else { } ; }");
        Parser parser = new Parser("main() { int aa, bb; aa = 10 ; if aa < 15 { bb = 6; write(bb+2); } else { } ; }", false);
        ProgramNode syntaxTree = parser.program();
        SymbolTable symbolTable = parser.getSymbolTable();
        SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
        semanticAnalyzer.checkIdentifiersDeclarations();
        semanticAnalyzer.assignDataTypes();
        semanticAnalyzer.checkAssignmentTypes();
        CodeGenerator codeGen = new CodeGenerator(syntaxTree, symbolTable);
        String actual = codeGen.generateCode();
        System.out.println("The actual result is:\n" + actual);
        String expected = ".data\n" +
                    " newline: .asciiz \"\\n\"\n" +
                    " aa :   .word   0\n" +
                    " bb :   .word   0\n" +
                    "\n" +
                    "\n" +
                    ".text\n" +
                    "\n" +
                    "main:\n" +
                    "addi   $sp, $sp, -4\n" +
                    "sw     $ra, 0 ($sp)\n" +
                    "li   $s0,   10\n" +
                    "sw    $s0,  aa\n" +
                    "lw  $t0,  aa\n" +
                    "li   $t1,   15\n" +
                    "slt  $s0, $t0, $t1\n" +
                    "beq  $s0, $zero elseCode1\n" +
                    "li   $s0,   6\n" +
                    "sw    $s0,  bb\n" +
                    "lw  $t0,  bb\n" +
                    "li   $t1,   2\n" +
                    "add    $a0,   $t0,   $t1\n" +
                    "li   $v0, 1\n" +
                    "syscall\n" +
                    "la   $a0, newline\n" +
                    "li   $v0, 4\n" +
                    "syscall\n" +
                    "j endif1\n" +
                    "elseCode1:\n" +
                    "endif1:\n" +
                    "lw    $ra, 0 ($sp)\n" +
                    "addi  $sp, $sp, 4\n" +
                    "jr    $ra\n";
        assertEquals( expected, actual);
    }
    
    /**
     * Test of writeCode method, of class CodeGenerator.
     */
    @Test
    public void testWriteCode_Three() {
        System.out.println("generate code - int temp(int qq); main() { int aa, bb; read(aa); bb = 7 + temp(aa);  write(bb); } int temp(int qq) {  write(qq); if (qq < 10) { temp(qq + 2); } else { return qq+ 3; }; }");
        String code = "int temp(int qq); main() { int aa, bb; read(aa); bb = 7 + temp(aa);  write(bb); } int temp(int qq) {  write(qq); if (qq < 10) { temp(qq + 2); } else { return qq+ 3; }; }";
        Parser parser = new Parser(code, false);
        ProgramNode syntaxTree = parser.program();
        SymbolTable symbolTable = parser.getSymbolTable();
        SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
        semanticAnalyzer.checkIdentifiersDeclarations();
        semanticAnalyzer.assignDataTypes();
        semanticAnalyzer.checkAssignmentTypes();
        CodeGenerator codeGen = new CodeGenerator(syntaxTree, symbolTable);
        String actual = codeGen.generateCode();
        System.out.println("The actual result is:\n" + actual);
        String expected = ".data\n" +
                    " newline: .asciiz \"\\n\"\n" +
                    " aa :   .word   0\n" +
                    " bb :   .word   0\n" +
                    " qq :   .word   0\n" +
                    "\n" +
                    "\n" +
                    ".text\n" +
                    "\n" +
                    "main:\n" +
                    "addi   $sp, $sp, -4\n" +
                    "sw     $ra, 0 ($sp)\n" +
                    "li $v0, 5\n" +
                    "syscall\n" +
                    "sw  $v0, aa\n" +
                    "li   $t0,   7\n" +
                    "lw  $s0,  aa\n" +
                    "jal temp\n" +
                    "move  $t1, $k0\n" +
                    "add    $s0,   $t0,   $t1\n" +
                    "sw    $s0,  bb\n" +
                    "lw  $a0,  bb\n" +
                    "li   $v0, 1\n" +
                    "syscall\n" +
                    "la   $a0, newline\n" +
                    "li   $v0, 4\n" +
                    "syscall\n" +
                    "lw    $ra, 0 ($sp)\n" +
                    "addi  $sp, $sp, 4\n" +
                    "jr    $ra\n" +
                    "\n" +
                    "\n" +
                    "temp:\n" +
                    "addi   $sp,$sp,-4\n" +
                    "sw     $ra,0($sp)\n" +
                    "sw  $s0,  qq\n" +
                    "lw  $a0,  qq\n" +
                    "li   $v0, 1\n" +
                    "syscall\n" +
                    "la   $a0, newline\n" +
                    "li   $v0, 4\n" +
                    "syscall\n" +
                    "lw  $a0,  qq\n" +
                    "li   $a1,   10\n" +
                    "slt  $s0, $a0, $a1\n" +
                    "beq  $s0, $zero elseCode1\n" +
                    "lw  $a0,  qq\n" +
                    "li   $a1,   2\n" +
                    "add    $s0,   $a0,   $a1\n" +
                    "jal temp\n" +
                    "j endif1\n" +
                    "elseCode1:\n" +
                    "lw  $a0,  qq\n" +
                    "li   $a1,   3\n" +
                    "add    $k0,   $a0,   $a1\n" +
                    "jal   tempEND\n" +
                    "endif1:\n" +
                    "tempEND:\n" +
                    "lw    $ra, 0($sp)\n" +
                    "addi  $sp,$sp,4\n" +
                    "jr    $ra\n";
        assertEquals( expected, actual);
    }
    
    /**
     * Test of writeCode method for statement of class CodeGenerator.
     */
    @Test
    public void testWriteCode_Statements_One() {
        System.out.println("generate code for statement - if statement");
        CodeGenerator codeGen = new CodeGenerator();
        IfStatementNode statementNode = new IfStatementNode();
        OperationNode opNode = new OperationNode(TokenType.AND);
        opNode.setLeft(new VariableNode("aa"));
        opNode.setRight(new ValueNode("5"));
        statementNode.setTest(opNode);
        String actual = codeGen.writeCode(statementNode, "$t0");
        System.out.println("The actual result is:\n" + actual);
        String expected = "lw  $t0,  aa\n" +
                "li   $t1,   5\n" +
                "seq  $s0,   $t0,   $zero\n" +
                "seq  $t2,   $t1,   $zero\n" +
                "seq  $s0, $s0,  $t2\n" +
                "beq  $s0, $zero elseCode1\n" +
                "j endif1\n" +
                "elseCode1:\n" +
                "endif1:\n";
        assertEquals( expected, actual);
    }
    
    /**
     * Test of writeCode method for statement of class CodeGenerator.
     */
    @Test
    public void testWriteCode_Statements_Two() {
        System.out.println("generate code for statement - while statement");
        CodeGenerator codeGen = new CodeGenerator();
        OperationNode opNode = new OperationNode(TokenType.LESSTHANOREQUAL);
        opNode.setLeft(new VariableNode("aa"));
        opNode.setRight(new VariableNode("bb"));
        WhileStatementNode statementNode = new WhileStatementNode();
        statementNode.setTest(opNode);
        CompoundStatementNode compNode = new CompoundStatementNode();
        DeclarationsNode decNode = new DeclarationsNode();
        compNode.setVariables(decNode);
        //compNode.addStatement(compNode);
        statementNode.setBody(compNode);
        String actual = codeGen.writeCode(statementNode, "$t0");
        System.out.println("The actual result is:\n" + actual);
        String expected = "startWhile0:\n" +
                "lw  $t0,  aa\n" +
                "lw  $t1,  bb\n" +
                "sle  $s0, $t0, $t1\n" +
                "beq  $s0, $zero endWhile0\n" +
                "j startWhile0\n" +
                "endWhile0:\n";
        assertEquals( expected, actual);
    }
}
