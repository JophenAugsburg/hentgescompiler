// SymbolTableTest.java
// Joseph Hentges

package symbolTable;

import util.SymbolKind;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * These are the Recognizer jUnit tests.
 * 
 * @author Joseph Hentges
 * February 27, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public class SymbolTableTest {
    
    /*-----------------------------------
            Add & GetKind Tests
    -----------------------------------*/
    
    /**
     * Test of add and getKind, of class SymbolTable.
     */
    @Test
    public void testAddGetKind_One() {
        System.out.println("add & getKind - func_one - FUNCTION_NAME");
        SymbolTable instance = new SymbolTable("NOT_FILE");
        instance.add("func_one", SymbolKind.FUNCTION_NAME);
        assertEquals(SymbolKind.FUNCTION_NAME, instance.getKind("func_one"));
    }
    
    /**
     * Test of add and getKind, of class SymbolTable.
     */
    @Test
    public void testAddGetKind_Two() {
        System.out.println("add & getKind - var1, func1, var3, func2");
        SymbolTable instance = new SymbolTable("NOT_FILE");
        instance.add("var1", SymbolKind.VARIABLE_NAME);
        instance.add("func1", SymbolKind.FUNCTION_NAME);
        instance.add("var3", SymbolKind.VARIABLE_NAME);
        instance.add("func2", SymbolKind.FUNCTION_NAME);
        assertEquals(SymbolKind.VARIABLE_NAME, instance.getKind("var1"));
        assertEquals(SymbolKind.FUNCTION_NAME, instance.getKind("func1"));
        assertEquals(SymbolKind.VARIABLE_NAME, instance.getKind("var3"));
        assertEquals(SymbolKind.FUNCTION_NAME, instance.getKind("func2"));
    }
    
    /**
     * Test of add and getKind, of class SymbolTable.
     */
    @Test
    public void testAddGetKind_Three() {
        System.out.println("add & getKind (add error) - func1, func1");
        SymbolTable instance = new SymbolTable("NOT_FILE");
        instance.add("func1", SymbolKind.FUNCTION_NAME);
        assertEquals(SymbolKind.FUNCTION_NAME, instance.getKind("func1"));
        try {
            instance.add("func1", SymbolKind.FUNCTION_NAME);
            fail("Fail...");
        } catch (RuntimeException e) {
            //  nothing
        }
    }
    
    
    /*-----------------------------------
            Add & Exists Tests
    -----------------------------------*/
    
    /**
     * Test of add and getKind, of class SymbolTable.
     */
    @Test
    public void testAddExists_One() {
        System.out.println("add & exists - func1");
        SymbolTable instance = new SymbolTable("NOT_FILE");
        instance.add("func1", SymbolKind.FUNCTION_NAME);
        assertTrue(instance.exists("func1"));
    }
    
    /**
     * Test of add and getKind, of class SymbolTable.
     */
    @Test
    public void testAddExists_Two() {
        System.out.println("add & exists (DNE) - func1");
        SymbolTable instance = new SymbolTable("NOT_FILE");
        assertFalse(instance.exists("func1"));
    }
    
}
