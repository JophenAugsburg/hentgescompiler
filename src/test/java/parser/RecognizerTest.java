// RecognizerTest.java
// Joseph Hentges

package parser;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import util.TokenType;
import util.SymbolKind;

/**
 * These are the Recognizer jUnit tests.
 * 
 * @author Joseph Hentges
 * February 06, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public class RecognizerTest {
    
    /*-----------------------------------
                Factor Tests
    -----------------------------------*/
    
    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void testFactor_One() {
        System.out.println("factor - ! 17");
        Recognizer instance = new Recognizer("! 17", false);
        instance.factor();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a factor");
    }
    
    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void testFactor_Two() {
        System.out.println("factor - A [a * b]");
        Recognizer instance = new Recognizer("A [a * b]", false);
        instance.factor();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a factor");
    }
    
    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void testFactor_Three() {
        System.out.println("factor (error) - (adb + b);");
        Recognizer instance = new Recognizer("(abd + b);", false);
        try {
            instance.factor();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.SEMICOLON, instance.lookahead.getType());
        }
        System.out.println("Parsed a factor");
    }
    
    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void testFactor_Four() {
        System.out.println("factor (error) - main(13 / 4)");
        Recognizer instance = new Recognizer("main(13 / 4)", false);
        try {
            instance.factor();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.MAIN, instance.lookahead.getType());
        }
        System.out.println("Parsed a factor");
    }
  
    
    /*-----------------------------------
           Simple Expression Tests
    -----------------------------------*/
    
    /**
     * Test of Simple_Expression method, of class Recognizer.
     */
    @Test
    public void testSimpleExpression_One() {
        System.out.println("simple_expression - a[12]");
        Recognizer instance = new Recognizer("a[12]", false);
        instance.simple_expression();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a simple_expression");
    }
    
    /**
     * Test of Simple_Expression method, of class Recognizer.
     */
    @Test
    public void testSimpleExpression_Two() {
        System.out.println("simple_expression - +a*b[3*4]");
        Recognizer instance = new Recognizer("+a*b[3*4]", false);
        instance.simple_expression();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a simple_expression");
    }
    
    /**
     * Test of Simple_Expression method, of class Recognizer.
     */
    @Test
    public void testSimpleExpression_Three() {
        System.out.println("simple_expression (error) - a+b*3[6]-");
        Recognizer instance = new Recognizer("a+b*3[6]-", false);
        try {
            instance.simple_expression();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.RIGHTBRACKET, instance.lookahead.getType());
        }
        System.out.println("Parsed a simple_expression");
    }
    
    /**
     * Test of Simple_Expression method, of class Recognizer.
     */
    @Test
    public void testSimpleExpression_Four() {
        System.out.println("simple_expression (error) - -3+b[4-4]*main");
        Recognizer instance = new Recognizer("-3+b[4-4]*main", false);
        try {
            instance.simple_expression();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.MAIN, instance.lookahead.getType());
        }
        System.out.println("Parsed a simple_expression");
    }
    
    /*-----------------------------------
                Statement Tests
    -----------------------------------*/
    
    /**
     * Test of Statement method, of class Recognizer.
     */
    @Test
    public void testStatement_One() {
        System.out.println("statement - return a[12]");
        Recognizer instance = new Recognizer("return a[12]", false);
        instance.getSymbolTable().add("a", SymbolKind.VARIABLE_NAME);
        instance.statement();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a statement");
    }
    
    /**
     * Test of Statement method, of class Recognizer.
     */
    @Test
    public void testStatement_Two() {
        System.out.println("statement - a = [a+b*3[6]-");
        Recognizer instance = new Recognizer("a = a+b*3+2", false);
        instance.getSymbolTable().add("a", SymbolKind.VARIABLE_NAME);
        instance.getSymbolTable().add("b", SymbolKind.VARIABLE_NAME);
        instance.getSymbolTable().add("3", SymbolKind.VARIABLE_NAME);
        instance.getSymbolTable().add("2", SymbolKind.VARIABLE_NAME);
        instance.statement();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a statement");
    }
    
    /**
     * Test of Statement method, of class Recognizer.
     */
    @Test
    public void testStatement_Three() {
        System.out.println("statement (error) - a = apple() % b[]");
        Recognizer instance = new Recognizer("a = apple(ape) % b[]", false);
        instance.getSymbolTable().add("a", SymbolKind.VARIABLE_NAME);
        instance.getSymbolTable().add("apple", SymbolKind.FUNCTION_NAME);
        instance.getSymbolTable().add("b", SymbolKind.VARIABLE_NAME);
        try {
            instance.statement();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.RIGHTBRACKET, instance.lookahead.getType());
        }
        System.out.println("Parsed a statement");
    }
    
    /**
     * Test of Statement method, of class Recognizer.
     */
    @Test
    public void testStatement_Four() {
        System.out.println("statement (error) - -3+b[4-4]*main");
        Recognizer instance = new Recognizer("while a < do a = a + 1", false);
        try {
            instance.statement();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.DO, instance.lookahead.getType());
        }
        System.out.println("Parsed a statement");
    }
    
    /*-----------------------------------
         Function Declaration Tests
    -----------------------------------*/
    
    /**
     * Test of Function Declaration method, of class Recognizer.
     */
    @Test
    public void testFunctionDeclaration_One() {
        System.out.println("function_declaration - void name(int a)");
        Recognizer instance = new Recognizer("void name(int a)", false);
        instance.function_declaration();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a function_declaration");
    }
    
    /**
     * Test of Function Declaration method, of class Recognizer.
     */
    @Test
    public void testFunctionDeclaration_Two() {
        System.out.println("function_declaration - int functionName (float id, int nd2)");
        Recognizer instance = new Recognizer("int functionName (float id)", false);
        instance.function_declaration();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a function_declaration");
    }
    
    /**
     * Test of Function Declaration method, of class Recognizer.
     */
    @Test
    public void testFunctionDeclaration_Three() {
        System.out.println("function_declaration (error) - void main(int a, float b)");
        Recognizer instance = new Recognizer("void main(int a, float b)", false);
        try {
            instance.function_declaration();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.MAIN, instance.lookahead.getType());
        }
        System.out.println("Parsed a function_declaration");
    }
    
    /**
     * Test of Function Declaration method, of class Recognizer.
     */
    @Test
    public void testFunctionDeclaration_Four() {
        System.out.println("function_declaration (error) - float functionName(int a, float b int d)");
        Recognizer instance = new Recognizer("float functionName(int a, float b, int 2d)", false);
        try {
            instance.function_declaration();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.NUMBER, instance.lookahead.getType());
        }
        System.out.println("Parsed a function_declaration");
    }
    
    /*-----------------------------------
             Declarations Tests
    -----------------------------------*/
    
    /**
     * Test of Declarations method, of class Recognizer.
     */
    @Test
    public void testDeclarations_One() {
        System.out.println("declarations - int a;");
        Recognizer instance = new Recognizer("int a;", false);
        instance.declarations();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a declarations");
    }
    
    /**
     * Test of Declarations method, of class Recognizer.
     */
    @Test
    public void testDeclarations_Two() {
        System.out.println("declarations - int a; float b; int d;");
        Recognizer instance = new Recognizer("int a; float b; int d;", false);
        instance.declarations();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a declarations");
    }
    
    /**
     * Test of Declarations method, of class Recognizer.
     */
    @Test
    public void testDeclarations_Three() {
        System.out.println("declarations (error) - int a; float d int a;");
        Recognizer instance = new Recognizer("int a; float d int a;", false);
        try {
            instance.declarations();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.INT, instance.lookahead.getType());
        }
        System.out.println("Parsed a declarations");
    }
    
    /**
     * Test of Declarations method, of class Recognizer.
     */
    @Test
    public void testDeclarations_Four() {
        System.out.println("declarations (error) - int a; float d; void main");
        Recognizer instance = new Recognizer("int a; float d; void main", false);
        try {
            instance.declarations();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.MAIN, instance.lookahead.getType());
        }
        System.out.println("Parsed a declarations");
    }
    
    /*-----------------------------------
                Program Tests
    -----------------------------------*/
    
    /**
     * Test of Program method, of class Recognizer.
     */
    @Test
    public void testProgram_One() {
        System.out.println("program - int func1(int a); void func2(float b); main(){int c;} int func3(int d){int s; s = 5;}");
        Recognizer instance = new Recognizer("int func1(int a); void func2(float b); main(){int c;} int func3(int d){int s; s = 5;}", false);
        instance.program();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a program");
    }
    
    /**
     * Test of Program method, of class Recognizer.
     */
    @Test
    public void testProgram_Two() {
        System.out.println("program - void func1(int a); int func2(void a); main(){int c;}");
        Recognizer instance = new Recognizer("void func1(int a); int func2(void b); main(){int c;}", false);
        instance.program();
        assertEquals(TokenType.ENDOFFILE, instance.lookahead.getType());
        System.out.println("Parsed a program");
    }
    
    /**
     * Test of Program method, of class Recognizer.
     */
    @Test
    public void testProgram_Three() {
        System.out.println("program (error) - main(float b){int a;}");
        Recognizer instance = new Recognizer("main(float b){int a;}", false);
        try {
            instance.program();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.FLOAT, instance.lookahead.getType());
        }
        System.out.println("Parsed a program");
    }
    
    /**
     * Test of Program method, of class Recognizer.
     */
    @Test
    public void testProgram_Four() {
        System.out.println("program (error) - int func1(int a); void func2(float b){}");
        Recognizer instance = new Recognizer("int func1(int a); void func2(float b); main(){int c;} int func1(int q){int w = q;}", false);
        try {
            instance.program();
            fail("Failed...");
        } catch (Error e) {
            assertEquals(TokenType.ASSIGN, instance.lookahead.getType());
        }
        System.out.println("Parsed a program");
    }
    
}
