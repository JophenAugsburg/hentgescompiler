// ScannerTest.java
// Joseph Hentges

package scanner;

import util.TokenType;
import java.io.StringReader;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * These are the Scanner jUnit tests.
 * 
 * @author Joseph Hentges
 * February 06, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */

public class ScannerTest {

    /**
     * Test of nextToken function.
     * Test the string "14 + 3; == %"
     */
    @Test
    public void testNextTokenHappyOne() throws Exception {
        System.out.println("Happy Test - 14 + 3; == %");
        String tokenString = "14 + 3; == %";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "14"));
        assertEquals(scanner.nextToken(), new Token(TokenType.PLUS, "+"));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "3"));
        assertEquals(scanner.nextToken(), new Token(TokenType.SEMICOLON, ";"));
        assertEquals(scanner.nextToken(), new Token(TokenType.EQUALS, "=="));
        assertEquals(scanner.nextToken(), new Token(TokenType.MOD, "%"));
    }
    
    /**
     * Test of nextToken function.
     * Test with a comment.
     * Test the string "return 25; // hi all \n int temp; /"
     */
    @Test
    public void testNextTokenHappyTwo() throws Exception {
        System.out.println("Happy Test - return 15; // hi all \n int temp; /");
        String tokenString = "return 15; // hi all \n int temp; /";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        assertEquals(scanner.nextToken(), new Token(TokenType.RETURN, "return"));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "15"));
        assertEquals(scanner.nextToken(), new Token(TokenType.SEMICOLON, ";"));
        assertEquals(scanner.nextToken(), new Token(TokenType.INT, "int"));
        assertEquals(scanner.nextToken(), new Token(TokenType.ID, "temp"));
        assertEquals(scanner.nextToken(), new Token(TokenType.SEMICOLON, ";"));
        assertEquals(scanner.nextToken(), new Token(TokenType.DIVIDE, "/"));
    }
    
    /**
     * Test of nextToken function.
     * Test the string "program int 14.4E1 13.323E-4 thisVar;"
     */
    @Test
    public void testNextTokenHappyThree() throws Exception {
        System.out.println("Happy Test - program int 14.4E1 13.323E-4 thisVar;");
        String tokenString = "program int 14.4E1 13.323E-4 thisVar;";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        assertEquals(scanner.nextToken(), new Token(TokenType.PROGRAM, "program"));
        assertEquals(scanner.nextToken(), new Token(TokenType.INT, "int"));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "14.4E1"));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "13.323E-4"));
        assertEquals(scanner.nextToken(), new Token(TokenType.ID, "thisVar"));
        assertEquals(scanner.nextToken(), new Token(TokenType.SEMICOLON, ";"));
    }
    
    /**
     * Test of nextToken function.
     * Test the string "int array = {1, 2};"
     */
    @Test
    public void testNextTokenHappyFour() throws Exception {
        System.out.println("Happy Test - int[] array = {1, 2};");
        String tokenString = "int array = {1, 2};";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        assertEquals(scanner.nextToken(), new Token(TokenType.INT, "int"));
        assertEquals(scanner.nextToken(), new Token(TokenType.ID, "array"));
        assertEquals(scanner.nextToken(), new Token(TokenType.ASSIGN, "="));
        assertEquals(scanner.nextToken(), new Token(TokenType.LEFTCURLYBRACKET, "{"));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "1"));
        assertEquals(scanner.nextToken(), new Token(TokenType.COMMA, ","));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "2"));
        assertEquals(scanner.nextToken(), new Token(TokenType.RIGHTCURLYBRACKET, "}"));
        assertEquals(scanner.nextToken(), new Token(TokenType.SEMICOLON, ";"));
    }
    
    /**
     * Test of nextToken function.
     * Test the string "&&||<="
     */
    @Test
    public void testNextTokenHappyFive() throws Exception {
        System.out.println("Happy Test - &&||<=");
        String tokenString = "&&||<=";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        assertEquals(scanner.nextToken(), new Token(TokenType.AND, "&&"));
        assertEquals(scanner.nextToken(), new Token(TokenType.OR, "||"));
        assertEquals(scanner.nextToken(), new Token(TokenType.LESSTHANOREQUAL, "<="));
    }
    
    /**
     * Test of nextToken function.
     * Tests against strings with illegal characters.
     * Test the string "#"
     */
    @Test
    public void testNextTokenSadOne() throws Exception {
        System.out.println("Sad Test - #");
        String tokenString = "#";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        try {
            scanner.nextToken();
            fail("Failed..");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * Test of nextToken function.
     * Tests against strings with illegal characters.
     * Test the string "return #;"
     */
    @Test
    public void testNextTokenSadTwo() throws Exception {
        System.out.println("Sad Test - return #;");
        String tokenString = "return #;";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        assertEquals(scanner.nextToken(), new Token(TokenType.RETURN, "return"));
        try {
            scanner.nextToken();
            fail("Failed..");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        assertEquals(scanner.nextToken(), new Token(TokenType.SEMICOLON, ";"));
    }
    
    /**
     * Test of nextToken function.
     * Tests against strings with illegal characters.
     * A bunch of symbols plus illegal characters.
     * Test the string "!=||&&*#(])"
     */
    @Test
    public void testNextTokenSadThree() throws Exception {
        System.out.println("Sad Test - !=||&&*#(])");
        String tokenString = "!=||&&*#(])";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        assertEquals(scanner.nextToken(), new Token(TokenType.NOTEQUAL, "!="));
        assertEquals(scanner.nextToken(), new Token(TokenType.OR, "||"));
        assertEquals(scanner.nextToken(), new Token(TokenType.AND, "&&"));
        assertEquals(scanner.nextToken(), new Token(TokenType.MULTIPLY, "*"));
        try {
            scanner.nextToken();
            fail("Failed..");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        assertEquals(scanner.nextToken(), new Token(TokenType.LEFTPARENTHESES, "("));
        assertEquals(scanner.nextToken(), new Token(TokenType.RIGHTBRACKET, "]"));
        assertEquals(scanner.nextToken(), new Token(TokenType.RIGHTPARENTHESES, ")"));
    }
    
    /**
     * Test of nextToken function.
     * Tests against strings with illegal characters.
     * Test the string "int[] temp = {1, 2, #, 3}"
     */
    @Test
    public void testNextTokenSadFour() throws Exception {
        System.out.println("Sad Test - int[] temp = {1, 2, #, 3}");
        String tokenString = "int[] temp = {1, 2, #, 3}";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        assertEquals(scanner.nextToken(), new Token(TokenType.INT, "int"));
        assertEquals(scanner.nextToken(), new Token(TokenType.LEFTBRACKET, "["));
        assertEquals(scanner.nextToken(), new Token(TokenType.RIGHTBRACKET, "]"));
        assertEquals(scanner.nextToken(), new Token(TokenType.ID, "temp"));
        assertEquals(scanner.nextToken(), new Token(TokenType.ASSIGN, "="));
        assertEquals(scanner.nextToken(), new Token(TokenType.LEFTCURLYBRACKET, "{"));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "1"));
        assertEquals(scanner.nextToken(), new Token(TokenType.COMMA, ","));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "2"));
        assertEquals(scanner.nextToken(), new Token(TokenType.COMMA, ","));
        try {
            scanner.nextToken();
            fail("Failed..");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        assertEquals(scanner.nextToken(), new Token(TokenType.COMMA, ","));
        assertEquals(scanner.nextToken(), new Token(TokenType.NUMBER, "3"));
        assertEquals(scanner.nextToken(), new Token(TokenType.RIGHTCURLYBRACKET, "}"));
    }
    
    /**
     * Test of nextToken function.
     * Tests against strings with illegal characters.
     * Test the string "program main; func test() {#"
     */
    @Test
    public void testNextTokenSadFive() throws Exception {
        System.out.println("Sad Test - program main; func test() {#");
        String tokenString = "program main; func test() {#";
        Scanner scanner = new Scanner(new StringReader(tokenString));
        assertEquals(scanner.nextToken(), new Token(TokenType.PROGRAM, "program"));
        assertEquals(scanner.nextToken(), new Token(TokenType.MAIN, "main"));
        assertEquals(scanner.nextToken(), new Token(TokenType.SEMICOLON, ";"));
        assertEquals(scanner.nextToken(), new Token(TokenType.FUNC, "func"));
        assertEquals(scanner.nextToken(), new Token(TokenType.ID, "test"));
        assertEquals(scanner.nextToken(), new Token(TokenType.LEFTPARENTHESES, "("));
        assertEquals(scanner.nextToken(), new Token(TokenType.RIGHTPARENTHESES, ")"));
        assertEquals(scanner.nextToken(), new Token(TokenType.LEFTCURLYBRACKET, "{"));
        try {
            scanner.nextToken();
            fail("Failed..");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
