// SemanticAnalyzerTest.java
// Joseph Hentges

package semanticAnalyzer;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import parser.Parser;
import symbolTable.SymbolTable;
import syntaxTree.ProgramNode;

/**
 * These are the Semantic Analyzer jUnit tests.
 * 
 * @author Joseph Hentges
 * March 15, 2020
 * April 9, 2020
 * Augsburg University
 * CSC 451 - Compilers
 */
public class SemanticAnalyzerTest {
    
    /*-------------------------------------
      CheckIdentifiersDeclarations Tests
    -------------------------------------*/

    /**
     * Test of checkIdentifiersDeclarations method of class SemanticAnalyzer.
     * Tests for true.
     */
    @Test
    public void testCheckIdentifiersDeclarations_One() {
        System.out.println("checkIdentifiersDeclarations - main(){int c;} int func3(int d){int s; s = 5;}");
        Parser parser = new Parser("main(){int c;} int func3(int d){int s; s = 5;}", false);
        ProgramNode syntaxTree = parser.program();
        SymbolTable symbolTable = parser.getSymbolTable();
        SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
        semanticAnalyzer.checkIdentifiersDeclarations();
        boolean expected = true;
        boolean actual = semanticAnalyzer.canWriteAssembly();
        System.out.println("The actual result is: " + actual);
        assertEquals( expected, actual);
    }
    
    /**
     * Test of checkIdentifiersDeclarations method of class SemanticAnalyzer.
     * Tests for false.
     */
    @Test
    public void testCheckIdentifiersDeclarations_Two() {
        System.out.println("checkIdentifiersDeclarations - main(){int c;} int func3(int d){int s; s = 5;}");
        Parser parser = new Parser("main(){int c;} int func3(int d){int s; s = 5 + q;}", false);
        ProgramNode syntaxTree = parser.program();
        SymbolTable symbolTable = parser.getSymbolTable();
        SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
        semanticAnalyzer.checkIdentifiersDeclarations();
        boolean expected = false;
        boolean actual = semanticAnalyzer.canWriteAssembly();
        System.out.println("The actual result is: " + actual);
        assertEquals( expected, actual);
    }
    
    /*-------------------------------------
             AssignDataTypes Tests
    -------------------------------------*/

    /**
     * Test of assignDataTypes method of class SemanticAnalyzer.
     */
    @Test
    public void testAssignDataTypes_One() {
        System.out.println("assignDataTypes - main(){float m, e; e = m + 1.2; }");
        Parser parser = new Parser("main(){float m, e; e = m + 1.2; }", false);
        ProgramNode syntaxTree = parser.program();
        SymbolTable symbolTable = parser.getSymbolTable();
        SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
        semanticAnalyzer.assignDataTypes();
        String expected = "Program:\n" +
                "|-- FunctionDefinitions\n" +
                "|-- Compound Statement\n" +
                "|-- --- Declarations\n" +
                "|-- --- --- Name: m | Datatype: FLOAT\n" +
                "|-- --- --- Name: e | Datatype: FLOAT\n" +
                "|-- --- Assignment\n" +
                "|-- --- --- Name: e | Datatype: FLOAT\n" +
                "|-- --- --- Operation: PLUS\n" +
                "|-- --- --- --- Name: m | Datatype: FLOAT\n" +
                "|-- --- --- --- Value: 1.2\n";
        String actual = syntaxTree.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print(actual);
        assertEquals( expected, actual);
    }
    
    /**
     * Test of assignDataTypes method of class SemanticAnalyzer.
     */
    @Test
    public void testAssignDataTypes_Two() {
        System.out.println("assignDataTypes - main(){float m, e; e = m + 1.2; } int func3(int d){int s; s = d + 1; }");
        Parser parser = new Parser("main(){float m, e; e = m + 1.2; } int func3(int d){int s; s = d + 1; }", false);
        ProgramNode syntaxTree = parser.program();
        SymbolTable symbolTable = parser.getSymbolTable();
        SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
        semanticAnalyzer.assignDataTypes();
        String expected = "Program:\n" +
                "|-- FunctionDefinitions\n" +
                "|-- --- Function: func3 returns INT\n" +
                "|-- --- --- Name: d | Datatype: INT\n" +
                "|-- --- --- Compound Statement\n" +
                "|-- --- --- --- Declarations\n" +
                "|-- --- --- --- --- Name: s | Datatype: INT\n" +
                "|-- --- --- --- --- Name: d | Datatype: INT\n" +
                "|-- --- --- --- Assignment\n" +
                "|-- --- --- --- --- Name: s | Datatype: INT\n" +
                "|-- --- --- --- --- Operation: PLUS\n" +
                "|-- --- --- --- --- --- Name: d | Datatype: INT\n" +
                "|-- --- --- --- --- --- Value: 1\n" +
                "|-- Compound Statement\n" +
                "|-- --- Declarations\n" +
                "|-- --- --- Name: m | Datatype: FLOAT\n" +
                "|-- --- --- Name: e | Datatype: FLOAT\n" +
                "|-- --- Assignment\n" +
                "|-- --- --- Name: e | Datatype: FLOAT\n" +
                "|-- --- --- Operation: PLUS\n" +
                "|-- --- --- --- Name: m | Datatype: FLOAT\n" +
                "|-- --- --- --- Value: 1.2\n";
        String actual = syntaxTree.indentedToString(0);
        System.out.println("The actual string is:");
        System.out.print(actual);
        assertEquals( expected, actual);
    }
    
    /*-------------------------------------
          CheckAssignmentTypes Tests
    -------------------------------------*/

    /**
     * Test of CheckAssignmentTypes method of class SemanticAnalyzer.
     * Test for false. - assigning an int variable with float expression.
     */
    @Test
    public void testCheckAssignmentTypes_One() {
        System.out.println("assignDataTypes - main(){float m; int n; n = m + 1.2; }");
        Parser parser = new Parser("main(){float m; int n; n = m + 1.2; }", false);
        ProgramNode syntaxTree = parser.program();
        SymbolTable symbolTable = parser.getSymbolTable();
        SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
        semanticAnalyzer.assignDataTypes();
        semanticAnalyzer.checkAssignmentTypes();
        boolean expected = false;
        boolean actual = semanticAnalyzer.canWriteAssembly();
        System.out.println("The actual result is: " + actual);
        assertEquals( expected, actual);
    }
    
    /**
     * Test of CheckAssignmentTypes method of class SemanticAnalyzer.
     * Test for true.
     */
    @Test
    public void testCheckAssignmentTypes_Two() {
        System.out.println("assignDataTypes - main(){float m, e; e = m + 1.2; } int func3(int d){int s; s = d + 1; }");
        Parser parser = new Parser("main(){float m, e; m = 1.2; e = m + 3.0; } int func3(int d){int s; s = d + 1; }", false);
        ProgramNode syntaxTree = parser.program();
        SymbolTable symbolTable = parser.getSymbolTable();
        SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer(syntaxTree, symbolTable);
        semanticAnalyzer.assignDataTypes();
        semanticAnalyzer.checkAssignmentTypes();
        boolean expected = true;
        boolean actual = semanticAnalyzer.canWriteAssembly();
        System.out.println("The actual result is: " + actual);
        assertEquals( expected, actual);
    }
    
}
